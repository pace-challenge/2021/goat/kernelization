#include "add_edge.hpp"

AddEdgeReduction::AddEdgeReduction ( int from, int to )
	: from ( from ), to ( to ) { }

void AddEdgeReduction::apply ( Graph & graph ) {
	graph.add_edge( from, to );
}

void AddEdgeReduction::revert ( Graph & graph, Solution * solution ) const {
	graph.remove_edge( from, to );
	if ( solution ) solution->add_edge( from, to );
}
