#pragma once

#include "../reduction_rule.hpp"

/*!
 * Adds an edge in an attempt to find the solution.
 */
class AddEdgeReduction : public ReductionRule<Graph> {
	public:

		AddEdgeReduction ( int from, int to );

		void apply ( Graph & graph ) ;

		void revert ( Graph & graph, Solution * solution ) const ;

	private:
		int from, to; /*!< The edded edge endpoints */
};

