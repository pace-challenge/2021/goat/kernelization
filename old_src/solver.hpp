#pragma once

#include "graph.hpp"
#include "solution.hpp"

template < class GRAPH_TYPE >
class Solver{
    public:

        /*!
         * Find the cluster editing solution for the graph g or return nullptr
         * if no solution exists.
         */
        virtual Solution * solve ( GRAPH_TYPE * g ) = 0;

};
