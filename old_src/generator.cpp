#include "graph.hpp"
#include "random_generator.hpp"

#include <iostream>
#include <cstdlib>
#include <ctime>


int main(int argc, char **argv){
    int min_args = 4;
    int max_args = 5;
    if(argc < min_args || max_args < argc){
        std::cerr << "Got " << argc - 1 << " arguments but expected [" << min_args - 1 << ", " << max_args - 1 << "]" << std::endl
                  << "Usage: " << argv[0] << " <vertices> <max_edge_flips> <starting_components> [seed]" << std::endl;
        return 1;
    }

    int seed;
    int n = atoi(argv[1]);
    int m = atoi(argv[2]);
    int k = atoi(argv[3]);
    bool seedProvided =  argc > 4;
    if(seedProvided){
        seed = atoi(argv[4]);
    }

    if(n < 0 || m < 0 || m > n*(n-1)/2 || k <= 0 || k > n){
        std::cerr << "Invalid parameters." << std::endl;
        return 1;
    }

    RandomGenerator randomGenerator = seedProvided ? RandomGenerator(seed) : RandomGenerator();
    Graph graph = randomGenerator.generate_graph(n, m, k);
    graph.print(std::cout);
    return 0;
}
