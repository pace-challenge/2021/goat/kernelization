#include <cmath>

#include "io/input_reader.hpp"
#include "critical_cliques.hpp"
#include "graph/radovan_graph.hpp"
#include "graph.hpp"
#include "reduction_rule.hpp"
#include "graph/conversion.hpp"

RadovanGraph convert_graph_to_weighted_graph_and_merge_critical_cliques(const Graph & g) {
    vector<vector<int>> g_critical_clique_decomposition = get_critical_clique_decomposition(g);
    RadovanGraph wg = convert_graph_to_radovan_graph(g);

    for(const vector<int> & cc : g_critical_clique_decomposition) {
        if(cc.size() == 1) continue;

        // after this merge, the newly created vertex will have number wg.n - 1
        wg.merge(cc[0], cc[1]);
        for(int i = 2; i < (int)cc.size(); ++i) {
            // we therefore want to merge next vertex in clique (surely was not touched yet) with the previously created vertex
            wg.merge(cc[i], wg.n - 1);
        }
    }

    return wg;
}

//-----------------------------------------------------------------------------

vector<vector<int>> find_complete_cliques(RadovanGraph & wg) {
    vector<int> wg_vertices = wg.get_valid_vertices();
    vector<vector<int>> true_closed_adjacency_lists(wg_vertices.size());

    for(int u_i = 0; u_i < (int)wg_vertices.size(); ++u_i) {
        int u = wg_vertices[u_i];
        true_closed_adjacency_lists[u].push_back(u);
        for(int v_i = u_i + 1; v_i < (int)wg_vertices.size(); ++v_i) {
            int v = wg_vertices[v_i];
            if(wg.get_edge_weight(u, v) > 0) {
                true_closed_adjacency_lists[u].push_back(v);
                true_closed_adjacency_lists[v].push_back(u);
            }
        }
    }

    for(vector<int> & tcal : true_closed_adjacency_lists) {
        sort(tcal.begin(), tcal.end());
    }
    sort(true_closed_adjacency_lists.begin(), true_closed_adjacency_lists.end());
    true_closed_adjacency_lists.push_back(vector<int>());

    vector<vector<int>> cliques;

    int i = 0;
    while(i < (int)true_closed_adjacency_lists.size()-1) {
        int j = i + 1;

        for(; j < (int)true_closed_adjacency_lists.size(); ++j) {
            if(true_closed_adjacency_lists[i] != true_closed_adjacency_lists[j]) break;
        }

        int clique_size = j - i;
        if(clique_size == (int)true_closed_adjacency_lists[i].size()) {
            // found a clique!
            cliques.push_back(true_closed_adjacency_lists[i]);
        }
        i = j;
    }
    return cliques;
}

struct ForbidEdgeReductionRule : public ReductionRule<RadovanGraph>{

    int f, t, original_w;

    ForbidEdgeReductionRule ( int f, int t ):f(f),t(t) { }

    virtual void apply ( RadovanGraph & graph ) {
        original_w = graph.get_edge_weight(f, t);
        graph.set_edge_weight(f, t, FORBIDDEN);
    }

    virtual void revert ( RadovanGraph & graph, Solution * solution ) const {
        graph.set_edge_weight(f, t, original_w);
        if(solution) solution->add_edge(f, t);
    }
};

struct RemoveDonePartsReductionRule : public ReductionRule<RadovanGraph>{

    vector<vector<int>> cliques;
    RadovanGraph wg;

    RemoveDonePartsReductionRule ( vector<vector<int>> &cliques ):cliques(cliques) { }

    virtual void apply ( RadovanGraph & graph ) {
        wg = graph;
        for(const vector<int> & clique : cliques) {
            for(int v : clique) graph.delete_vertex(v);
        }
    }

    virtual void revert ( RadovanGraph & graph, Solution * solution ) const {
        graph = wg; // todo, does this work?
        (void)solution; // solution ignored, as removing cliques doesn't change it
    }
};

/**
 * Naive implementation of reduction of unaffordable edge operations.
 * Through induced_permanent_cost and induced_forbidden_cost we are able to check if the operations exceeds budget k. If it does, we may deterministically decide what to do.
 * This implementation runs in O(n^4)-ish as it is naive.
 */
vector<ReductionRule<RadovanGraph>*> reduction_unaffordable_edge_operations(RadovanGraph & wg, int & k) {
    vector<ReductionRule<RadovanGraph>*> reductions;

    vector<int> wg_vertices = wg.get_valid_vertices();
    // naively check all pairs
    for(int u_i = 0; u_i < (int)wg_vertices.size(); ++u_i) {
        int u = wg_vertices[u_i];
        for(int v_i = u_i + 1; v_i < (int)wg_vertices.size(); ++v_i) {
            int v = wg_vertices[v_i];

            if(wg.get_edge_weight(u, v) == FORBIDDEN) {
                continue;
            }
            if(wg.get_edge_weight(u, v) <= 0) {
                continue;
            }

            int ipc = wg.induced_permanent_cost(u, v);
            int ifc = wg.induced_forbidden_cost(u, v);

            // if both exceed budget, instance is unsolvable
            if(ipc > k && ifc > k) {
                k = -1;
                return reductions; // todo, instance is UNSOLVABLE ... end here
            }

            // setting it to permanent would exceed budget, so it must be set to forbidden
            if(ipc > k) {
                // delete if necessary = if weight is positive, we pay for deletion, otherwise we do not
                k -= std::max(wg.get_edge_weight(u, v), 0);
                auto *red = new ForbidEdgeReductionRule(u, v);
                red->apply(wg);
                reductions.push_back(red);
            }
            // setting it to forbidden would exceed budget, so it must be set to permanent, which means we have to merge the vertices and we pay the cost of merge
            else if(ifc > k) {
                k -= wg.merge(u, v);
                // just to be safe, we will immediately return, as the wg_vertices are no longer valid
                return reductions;
            }

        }
    }

    return reductions;
}

vector<ReductionRule<RadovanGraph>*> reduction(RadovanGraph & wg, int & k) {
    vector<ReductionRule<RadovanGraph>*> reductions;
    while(1) {
        // std::cout<<"******************* 11111 **********************"<<std::endl;
        // wg.print(std::cout);
        wg = wg.consolidate();
        // std::cout<<"******************* 22222 **********************"<<std::endl;
        // wg.print(std::cout);
        vector<vector<int>> cliques = find_complete_cliques(wg);
        if(cliques.size() != 0) { // can apply clique deletion reduction
            auto *red = new RemoveDonePartsReductionRule(cliques);
            reductions.push_back(red);
            red->apply(wg);
            continue;
        }
        auto rds = reduction_unaffordable_edge_operations(wg, k);
        if(rds.size()!=0) {
            for(auto r : rds) reductions.push_back(r);
            if(k<0) {
                k = -1;
                return reductions;
            }
            continue;
        }
        break;
    }
    return reductions;
}

//-----------------------------------------------------------------------------

vector<vector<int>> find_connected_components_using_only_true_edges(const RadovanGraph & wg) {
    vector<int> vertices = wg.get_valid_vertices();
    int components_cnt = 0;
    vector<int> components(vertices.size(), -1);

    for(int i = 0; i < (int)vertices.size(); ++i) {
        if(components[i] == -1) {
            vector<int> _stack;
            _stack.push_back(i);
            components[i] = components_cnt;

            while(_stack.size()) {
                int v_i = _stack.back();
                _stack.pop_back();

                for(int v_j = 0; v_j < (int)vertices.size(); ++v_j) {
                    if(v_i == v_j) continue;
                    if(wg.get_edge_weight(vertices[v_i], vertices[v_j]) <= 0) continue;
                    if(components[v_j] == -1) {
                        components[v_j] = components_cnt;
                        _stack.push_back(v_j);
                    }
                }
            }

            components_cnt++;
        }
    }

    vector<vector<int>> cc(components_cnt);
    for(int i = 0; i < (int)vertices.size(); ++i) {
        cc[components[i]].push_back(vertices[i]);
    }

    return cc;
}

//-----------------------------------------------------------------------------

double compute_branching_factor_comparator_for_b1_b2(double b1, double b2) {
    if(b1>b2)std::swap(b1, b2);

    double bfc = 1.0;
    for(int i = 0; i < 40; ++i) {
        if(std::pow(bfc, b2) > std::pow(bfc, b2 - b1) + 1) break;
        bfc += 0.05;
    }
    return bfc;
}

double compute_branching_factor_comparator_for_edge(const RadovanGraph & wg, int u, int v) {
    assert(wg.vertices_valid[u]);
    assert(wg.vertices_valid[v]);
    assert(wg.get_edge_weight(u, v) > 0);

    // one branch is to delete the edge = set to forbidden => in that case we pay the cost of deleting the edge
    int b1 = wg.get_edge_weight(u, v);

    // second branch is merge the edge = set to permanent => in that case we pay the cost of mergin, which is induced_permanent_cost
    int b2 = wg.induced_permanent_cost(u, v);

    return compute_branching_factor_comparator_for_b1_b2(b1, b2);
}

struct PossibleEdge{
    int f, t;
    bool found;
};
// naively find a good conflict triple vuw and return conflict edge uv
PossibleEdge get_good_conflict_edge(const RadovanGraph & wg) {
    vector<int> wg_vertices = wg.get_valid_vertices();
    double best_bfc = 1000;
    PossibleEdge best_edge = {-1,-1,false};

    for(int i=0; i < (int)wg_vertices.size(); ++i) {
        for(int j=i+1; j < (int)wg_vertices.size(); ++j) {
            int v = wg_vertices[i];
            int w = wg_vertices[j];
            if(wg.get_edge_weight(v, w) > 0) continue;

            for(int k=0; k < wg.n; ++k) {
                if(k==i || k == j) continue;
                int u = wg_vertices[k];

                // found a conflict triple vuw!
                if(wg.get_edge_weight(u, v) > 0 && wg.get_edge_weight(u, w) > 0) {
                    double bfc = compute_branching_factor_comparator_for_edge(wg, u, v);
                    if(bfc < best_bfc) {
                        best_bfc = bfc;
                        best_edge = {u, v, true};
                    }
                }
            }
        }
    }

    return best_edge;
}

Solution* iteratively_find_optimal_k(const RadovanGraph & wg, int limit_k);

Solution* branch(const RadovanGraph & _wg, int k) {
    RadovanGraph wg = _wg;

    // first step in the search node is reduction
    reduction(wg, k);

    // if budget exceeding, we have no solution
    if(k < 0) return nullptr;

    auto conflict_edge_r = get_good_conflict_edge(wg);
    // no conflict edge, we have a solution
    if(conflict_edge_r.found == false) {
        return new Solution();
    }

    // otherwise, we have a good conflict edge uv on which we branch
    int u = conflict_edge_r.f;
    int v = conflict_edge_r.t;

    // find connected components and solve the problem for each separately
    vector<vector<int>> connected_components = find_connected_components_using_only_true_edges(wg);
    if(connected_components.size() > 1) {
        Solution* merged = new Solution();
        for(const vector<int> & cc : connected_components) {
            Solution* sol = iteratively_find_optimal_k(wg.get_induced_subgraph(cc), k);
            // the optimal solution of connected component was too big, there is no solution
            if(sol == nullptr) return nullptr;
            map<int,int> mapping;
            for(int i=0; i<(int)connected_components.size(); ++i){
                for(int j : connected_components[i]){
                    mapping[j] = i;
                }
            }
            sol->map_vertices(mapping);
            k -= sol->size();
            merged->merge(*sol);
            delete sol;
        }
        assert(k>=0);
        return merged;
    }

    // otherwise we branch
    Solution *s;
    RadovanGraph wg1 = wg;
    int k1 = k - wg1.get_edge_weight(u, v);
    wg1.set_edge_weight(u, v, FORBIDDEN);
    if(k1 >= 0){
        s = branch(wg1, k1);
        if(s){ s->add_edge(u, v); return s; }
    }

    RadovanGraph wg2 = wg;
    int k2 = k - wg2.merge(u, v);
    if(k2 >= 0){
       s = branch(wg2, k2);
       if(s){ s->add_edge(u, v); return s; }
    }

    return nullptr;
}

Solution* iteratively_find_optimal_k(const RadovanGraph & wg, int limit_k) {
    // option to limit the search for k
    if(limit_k == -1) limit_k = wg.n*wg.n;
    Solution *s;
    for(int k = 0; k <= limit_k; ++k) {
        if((s=branch(wg, k))) return s;
    }
    return nullptr;
}

int main() {
    InputReader in;
    Graph g = *in.read( std::cin );
    RadovanGraph wg = convert_graph_to_weighted_graph_and_merge_critical_cliques(g);
    Solution* sol = iteratively_find_optimal_k(wg, -1);
    std::cout << (*sol) << std::endl;
}
