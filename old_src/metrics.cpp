#include <queue>
#include <set>
#include <vector>
#include <utility>
#include <limits>
#include <iostream>
#include <algorithm>

#include "metrics.hpp"
#include "graph.hpp"

using namespace std;


void recalculate_edges(const vector<int> & parents, int source, int sink, set<pair<int, int>> & used_edges){
    int current = sink;
    while (current != source){
        int parent = parents[current];
        // exist in opposite direction
        if(used_edges.count(make_pair(current, parent)) > 0){
            used_edges.erase(make_pair(current, parent));
        } else if(used_edges.count(make_pair(parent, current)) > 0){
            throw "this should not happen";
        } else {
            used_edges.insert(make_pair(parent, current));
        }
        current = parent;
    }   
}

// straight going to hell for updating used_edges, and returning only if path was found
bool find_enhancing_path(Graph graph, int source, int sink, set<pair<int, int>> & used_edges){
    set<int> visited;
    vector<int> parent(graph.get_vertices_count());
    queue<int> q;
    q.push(source);
    parent[source] = source;
    visited.insert(source);
    while(!q.empty()){
        int current = q.front();
        q.pop();
        for(int i: graph.get_adjacency_list(current)){
            if(visited.count(i) == 0 && used_edges.count(make_pair(current, i)) == 0){
                parent[i] = current;
                // we check sink as neigbour so it wont go through the whole queue 
                if(i == sink){
                    // commiting changes to used_edges
                    recalculate_edges(parent, source, sink, used_edges);
                    return true;
                }
                visited.insert(i);
                q.push(i);
            }
        }
    }
    return false;
}

// capacity is implicitly 1
// max_k specifies maximal solution that we need to calculate
int ford_fulkerson(Graph graph, int source, int sink, int max_k){
    set<pair<int, int>> used_edges;
    int k = 0;
    while(k < max_k && find_enhancing_path(graph, source, sink, used_edges)){
        ++k;
    }
    return k;
}


int calculate_edge_connectivity(Graph graph){
    int min_connectivity = std::numeric_limits<int>::max();
    // we can fix one node, so we fix the sink
    for(size_t i = 1; i < graph.get_vertices_count(); ++i){
        min_connectivity = ford_fulkerson(graph, i, 0, min_connectivity);
    }
    return min_connectivity;
}


vector<int> get_degrees(Graph graph){
    vector<int> degrees;
    for(size_t i = 0; i < graph.get_vertices_count(); ++i){
        degrees.push_back(graph.get_adjacency_list(i).size());
    }
    return degrees;
}

tuple<int, int, int> calculate_min_max_median_degree(Graph graph){
    vector<int> degrees = get_degrees(graph);
    sort(degrees.begin(), degrees.end());
    int mid = degrees.size()/2;
    return make_tuple(degrees[0], degrees[degrees.size() - 1], degrees[mid]);
}