#include "radovan_graph.hpp"

RadovanGraph::RadovanGraph() : n(0), n_valid(0), vertices_valid(0, true), weighted_adj_matrix() { }

RadovanGraph::RadovanGraph(int n, const vector<vector<int>> & weighted_adj_matrix) :
    n(n), n_valid(n), vertices_valid(n, true), weighted_adj_matrix(weighted_adj_matrix) { }

int RadovanGraph::get_edge_weight(int u, int v) const {
    assert(vertices_valid[u]);
    assert(vertices_valid[v]);
    if(u<v) std::swap(u, v);
    if(u==v) return FORBIDDEN;
    return weighted_adj_matrix[u][v];
}

void RadovanGraph::set_edge_weight(int u, int v, int s) {
    assert(vertices_valid[u]);
    assert(vertices_valid[v]);
    assert(u!=v);
    if(u<v) std::swap(u, v);
    weighted_adj_matrix[u][v] = s;
}

vector<int> RadovanGraph::get_valid_vertices() const {
    vector<int> vertices;
    for(int v = 0; v < n; ++v) {
        if(vertices_valid[v]) vertices.push_back(v);
    }
    return vertices;
}

bool RadovanGraph::contains_edge(int u, int v) const {
    if(u<v) std::swap(u, v);
    return weighted_adj_matrix[u][v] > 0;
}

void RadovanGraph::flip_edge(int u, int v){
    if(u<v) std::swap(u, v);
    weighted_adj_matrix[u][v] = -weighted_adj_matrix[u][v];
}

vector<int> RadovanGraph::neighbors(int u) const {
    vector<int> vertices;
    for(int v = 0; v < n; ++v) {
        if(v!=u && vertices_valid[v] && contains_edge(u, v)) {
            vertices.push_back(v);
        }
    }
    return vertices;
}

void RadovanGraph::delete_vertex(int v) {
    assert(vertices_valid[v]);
    for(int w = v+1; w<n; ++w) {
        if(vertices_valid[w] == false) continue;
        set_edge_weight(v, w, FORBIDDEN);
    }
    vertices_valid[v] = false;
    n_valid--;
}

RadovanGraph RadovanGraph::get_induced_subgraph(const vector<int> & vertices) const {
    for(int v : vertices) assert(vertices_valid[v]);

    int new_n = vertices.size();
    vector<vector<int>> new_weighted_adj_matrix;
    for(int i = 0; i < new_n; ++i) {
        new_weighted_adj_matrix.push_back(vector<int>(i, 0));
    }

    RadovanGraph wg_induced(new_n, new_weighted_adj_matrix);
    for(int i = 0; i < new_n; ++i) {
        for(int j = i+1; j < new_n; ++j) {
            wg_induced.set_edge_weight(i, j, get_edge_weight(vertices[i], vertices[j]));
        }
    }

    return wg_induced;
}

int RadovanGraph::merge(int u, int v) {
    assert(vertices_valid[u]);
    assert(vertices_valid[v]);
    assert(u!=v);
    assert(get_edge_weight(u, v) > 0);
    if(u>v)std::swap(u, v);

    // the cost of this operation to parameter k is ipc
    int cost = induced_permanent_cost(u, v);

    // we create new space for new weights for the new vertex
    weighted_adj_matrix.push_back(vector<int>());
    for(int w = 0; w < n; ++w) {
        // these vertices will be deleted, so the edge weight does not exist and is forbidden
        if(w==u || w == v) {
            weighted_adj_matrix[n].push_back(FORBIDDEN);
            continue;
        }
        // if w is not valid, there is no edge in the graph from n to w, so the edge is forbidden
        if(vertices_valid[w] == false) {
            weighted_adj_matrix[n].push_back(FORBIDDEN);
            continue;
        }

        int uw_weight = get_edge_weight(u, w);
        int vw_weight = get_edge_weight(v, w);
        int new_weight;

        // naively redefine addition of weights
        if(uw_weight == FORBIDDEN || vw_weight == FORBIDDEN) {
            new_weight = FORBIDDEN;
        } else {
            new_weight = uw_weight + vw_weight;
        }

        // we push the new weight
        weighted_adj_matrix[n].push_back(new_weight);
        // and set the old weights to forbidden as they do not exist anymore
        set_edge_weight(u, w, FORBIDDEN);
        set_edge_weight(v, w, FORBIDDEN);
    }

    // number of stored vertices increases
    n++;
    // number of valid vertices decreases - we delete two and add one
    n_valid--;
    // the new vertex is valid
    vertices_valid.push_back(true);
    // the deleted vertices are invalid
    vertices_valid[u] = false;
    vertices_valid[v] = false;

    return cost;
}

int RadovanGraph::induced_forbidden_cost(int u, int v) const {
    assert(vertices_valid[u]);
    assert(vertices_valid[v]);
    assert(u!=v);
    assert(get_edge_weight(u, v) != FORBIDDEN);

    // edge should be forbidden, therefore we pay only if the uv weight is positive (the edge exists and it should not)
    int ifc = std::max(0, get_edge_weight(u, v));

    for(int w = 0; w < n; ++w) {
        if(vertices_valid[w] == false) continue;
        if(w==u || w==v) continue;

        int uw_weight = get_edge_weight(u, w);
        int vw_weight = get_edge_weight(v, w);

        // if this holds, then w is a common neigbor of u and v, as the edge uv will not be in the result, we must resolve this conflict
        if(uw_weight > 0 && vw_weight > 0) {
            ifc += std::min(uw_weight, vw_weight);
        }
    }

    return ifc;
}

int RadovanGraph::induced_permanent_cost(int u, int v) const {
    assert(vertices_valid[u]);
    assert(vertices_valid[v]);
    assert(u!=v);
    assert(get_edge_weight(u, v) != FORBIDDEN);

    // edge should be permanent, therefore we pay only if the uv weight is negative (the edge does not exist and it should)
    int ipc = std::max(0, -get_edge_weight(u, v));

    for(int w = 0; w < n; ++w) {
        if(vertices_valid[w] == false) continue;
        if(w==u || w==v) continue;

        int uw_weight = get_edge_weight(u, w);
        int vw_weight = get_edge_weight(v, w);

        // if this holds, then w is a neigbor of u and v but not both, as the edge uv will be in the result, we must resolve this conflict
        // w belongs to the symmetric difference of true neighborhoods of u and v
        if((uw_weight > 0 && vw_weight <= 0) || (uw_weight <= 0 && vw_weight > 0)) {
            ipc += std::min(std::abs(uw_weight), std::abs(vw_weight));
        }
    }

    return ipc;
}

RadovanGraph RadovanGraph::consolidate() const {
    int new_n = 0;
    vector<int> vertices_map(n, -1);
    for(int u = 0; u < n; ++u) {
        if(vertices_valid[u]) vertices_map[u] = new_n++;
    }
    assert(new_n == n_valid);
    vector<vector<int>> new_weighted_adj_matrix;
    for(int i = 0; i < new_n; ++i) {
        new_weighted_adj_matrix.push_back(vector<int>(i, 0));
    }

    RadovanGraph wg(new_n, new_weighted_adj_matrix);

    for(int u = 0; u < n; ++u) {
        if(vertices_valid[u] == false) continue;
        for(int v = u + 1; v < n; ++v) {
            if(vertices_valid[v] == false) continue;
            wg.set_edge_weight(vertices_map[u], vertices_map[v], get_edge_weight(u, v));
        }
    }

    return wg;
}

void RadovanGraph::print(ostream & out) const {
    out<<"n: "<<n<<std::endl;
    out<<"n_valid: "<<n_valid<<std::endl;
    out<<"valid_vertices:";
    for(int v : get_valid_vertices())out<<" "<<v;
    out<<std::endl;
    out<<"weighted_adj_matrix: "<<std::endl;
    for(int i = 0; i < n; ++i) {
        if(!vertices_valid[i]) {
            out<<std::endl;
            continue;
        }
        out<<i<<": ";
        for(int j = 0; j < i; ++j) {
            // f stands for forbidden
            if(weighted_adj_matrix[i][j] == FORBIDDEN) out<<"f";
            else out<<weighted_adj_matrix[i][j];
            out<<"\t";
        }
        out<<std::endl;
    }
}
