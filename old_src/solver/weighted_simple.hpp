#pragma once

#include "../graph/weighted_graph.hpp"
#include "../solution.hpp"
#include "../solver.hpp"

/*!
 * Solves the instance in O(3^K * N^3). Returns the first found solution
 * which uses at most K edits -- doesn't return the minimal solution!
 */
class WeightedSimpleSolver : public Solver<WeightedGraph>{
    public:

        WeightedSimpleSolver ( int K );

        virtual Solution * solve ( WeightedGraph * g );

    private:
        Solution * flip_edge ( WeightedGraph * g, int i, int j );
        int K;
        int depth;
};
