#pragma once


#include <chrono>
using namespace std::chrono;

class Timer{
    public:
        void start(){
            start_time = high_resolution_clock::now();
        }

        int stop(){
            auto stop_time = high_resolution_clock::now();
            return duration_cast<microseconds>(stop_time - start_time).count();
        }

    private:
        time_point<high_resolution_clock> start_time;
};

struct ContextTimer {
    long long & counter_ms;
    std::chrono::time_point<std::chrono::high_resolution_clock> start;

    ContextTimer(long long & counter_ms) : counter_ms(counter_ms), start(std::chrono::high_resolution_clock::now()) {
    }

    ~ContextTimer() {
        auto elapsed = std::chrono::high_resolution_clock::now() - start;
        long long microseconds = std::chrono::duration_cast<std::chrono::microseconds>(elapsed).count();
        counter_ms += microseconds;
    }
};

