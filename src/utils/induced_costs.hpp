#pragma once

#include "../graph/weighted_graph.hpp"
#include <unordered_map>
#include <unordered_set>

struct InducedCosts {
    InducedCosts(const WeightedGraph & g);

    int get_icp(int u, int v) const;
    int get_icf(int u, int v) const;

    // this must be called before the edge was set to forbidden in the graph!
    void update_edge_forbidden_before(int u, int v, const WeightedGraph & g);

    // merge is two part process one happens before the edge is merged and another after
    // this must be called before the edge is merged to v!
    void update_edge_merge_u_to_v_before(int u, int v, const WeightedGraph & g);
    // this must be called after the edge is merged to v!
    void update_edge_merge_u_to_v_after(int u, int v, const WeightedGraph & g);


    int compute_icp(int u, int v, const WeightedGraph & g) const;
    int compute_icf(int u, int v, const WeightedGraph & g) const;

    void check(const WeightedGraph & g) const;

private:
    // std::unordered_set<int> valid_vertices;
    // std::unordered_map<int, int> icp;
    // std::unordered_map<int, int> icf;

    vector<int> icp;
    vector<int> icf;

    int _get_edge_idx(int u, int v) const;
    int _add(int a, int b) const;

    void _set_icp(int u, int v, int val);
    void _set_icf(int u, int v, int val);

    void _update_edge_forbidden_icf(int u, int v, const WeightedGraph & g);
    void _update_edge_forbidden_icp_ux(int u, int v, const WeightedGraph & g);
    void _update_edge_forbidden_icp_vx(int u, int v, const WeightedGraph & g);

    void _update_edge_merge_u_to_v_icf_uvwx(int u, int v, int w, int x, const WeightedGraph & g);
    void _update_edge_merge_u_to_v_icp_uvwx(int u, int v, int w, int x, const WeightedGraph & g);
};

void perform_induced_costs_graph_edge_forbidden(int u, int v, WeightedGraph & g, InducedCosts & ic);
void perform_induced_costs_graph_edge_merge_u_to_v(int u, int v, WeightedGraph & g, InducedCosts & ic);
