#pragma once

#include "../graph/weighted_graph.hpp"
#include "../graph/neighbours_graph.hpp"
#include <vector>
#include <map>

using std::map;
using std::vector;

/*!
 * Recursive DFS to find all vertices reachable from vertex `v`.
 *
 * For {@internal} use only.
 */
void find_component_rec ( const WeightedGraph & graph, int v, vector<int> & c, map<int,bool> &found );

/*!
 * Utility function which determines all graph components.
 *
 * \param[in] graph
 *
 * \return vector of components, each component consists of vector of its vertices.
 */
vector<vector<int>> get_components ( const WeightedGraph & graph );


/*!
 * Propper DFS to find all vertices reachable from vertex `v`.
 *
 * For {@internal} use only.
 */
void find_component ( const NeighboursGraph & graph, int v, vector<int> & c, map<int,bool> &found );

/*!
 * Utility function which determines all graph components.
 *
 * \param[in] graph
 *
 * \return vector of components, each component consists of vector of its vertices.
 */
vector<vector<int>> get_components ( const NeighboursGraph & graph );

