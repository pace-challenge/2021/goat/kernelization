#pragma once

#include <vector>
#include "../graph/weighted_graph.hpp"

using std::vector;
using std::pair;

struct MinCut {
    int value;
    vector<pair<int,int>> edges;
    MinCut(int value, vector<pair<int,int>> edges) : value(value), edges(edges) {}
};

MinCut min_cut_of_induced_graph(const WeightedGraph & g, const vector<int> & vs);
