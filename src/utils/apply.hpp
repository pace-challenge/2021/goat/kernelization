#pragma once

#include "../graph/weighted_graph.hpp"
#include "../node/node.hpp"
#include "../reduction_rule/reduction_rule.hpp"

Node * apply_directly(WeightedGraph * graph, Bound & b, ReductionRule<WeightedGraph> *rule, Node::node_factory factory){
    rule->apply(graph, b);
    return factory(graph, b);
}

Node * apply_to_copy(WeightedGraph * graph, Bound b, ReductionRule<WeightedGraph> *rule, Node::node_factory factory){
    return apply_directly(graph->copy(), b, rule, factory);
}

