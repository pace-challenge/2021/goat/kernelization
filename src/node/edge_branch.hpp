#pragma once

#include<functional>

#include "../graph/weighted_graph.hpp"
#include "node.hpp"
#include "../reduction_rule/merge.hpp"

int induced_cost_permanent(WeightedGraph * g, int u, int v);

double compute_branching_number(double a, double b);

struct EdgeBranchNode : public Node {

    EdgeBranchNode(WeightedGraph *& graph, Bound bound);
    EdgeBranchNode(WeightedGraph *&& graph, Bound bound);
    EdgeBranchNode(WeightedGraph *& graph, Bound bound, Node::node_factory new_node_factory);
    EdgeBranchNode(WeightedGraph *&& graph, Bound bound, Node::node_factory new_node_factory);

    virtual ~EdgeBranchNode();

    virtual EvalResult evaluate();

    virtual EvalResult child_finished(Node *child);

    virtual EvalResult all_children_finished();

    private:
        int u, v;
        MergeReduction * mergeRule;
        node_factory new_node_factory;

        WeightedGraph * copy_then_merge(const WeightedGraph * source, int u, int v, Bound & bound);
};
