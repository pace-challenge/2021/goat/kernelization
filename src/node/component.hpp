#pragma once

#include <functional>
#include <vector>

#include "node.hpp"
#include "../graph/weighted_graph.hpp"
#include "../utils/get_components.hpp"

using std::vector;
using std::function;



struct ComponentNode : public Node {

    ComponentNode(WeightedGraph *& graph, Bound bound, Node::node_factory children_factory);
    ComponentNode(WeightedGraph *&& graph, Bound bound, Node::node_factory children_factory);

    virtual EvalResult evaluate();

    virtual EvalResult child_finished(Node *child);

    virtual EvalResult all_children_finished();

    void recompute_bounds(Node *bound_children = nullptr);

    virtual void notify_parent_about_bound_change(Node *child, Bound new_bounds);

    virtual void notify_child_about_bound_change(Bound new_bounds);

    private:
        Node::node_factory children_factory;
};
