#pragma once

#include "../graph/weighted_graph.hpp"
#include "node.hpp"

#include <vector>
#include <tuple>

using std::vector;
using std::tuple;

bool is_cherry ( int u, int v, int w, const WeightedGraph * G );

vector<int> find_cherry ( const WeightedGraph * g ) ;

vector<tuple<int,int,int>> find_cherries ( const WeightedGraph * G );

WeightedGraph *copy_and_flip(const WeightedGraph*g, int from, int to);


struct GreedyCherryNode : public Node {

    GreedyCherryNode(WeightedGraph *& graph, int exact_bound);
    GreedyCherryNode(WeightedGraph *&& graph, int exact_bound);

    virtual EvalResult evaluate();

    virtual EvalResult child_finished(Node *child);

    virtual EvalResult all_children_finished();

    private:
        vector<int> cherry;
};
