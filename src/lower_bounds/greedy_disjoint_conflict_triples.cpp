#include "greedy_disjoint_conflict_triples.hpp"
#include "../debug/debug.hpp"
#include <set>
#include <tuple>
#include <cassert>
#include <algorithm>
#include <unordered_set>
#include <unordered_map>
#include <map>
#include <set>
#include "../utils/bitset.hpp"
#include "../utils/timer.hpp"

long long _lb_timer1 = 0;
long long _lb_timer2 = 0;
long long _lb_timer3 = 0;


using std::set;
using std::map;
using std::pair;

using ConflictTriple = std::pair<int, std::tuple<int,int,int>>;

vector<ConflictTriple> get_all_conflict_triples(const WeightedGraph & wg) {
    vector<std::pair<int, std::tuple<int,int,int>>> conflict_triples;
    const auto & vs = wg.all_vertices();

    int v_max = 0;
    for(int v : vs) {
        if(v >= v_max) v_max = v + 1;
    }

    vector<int> v_to_in_v_map(v_max, -1);
    vector<int> in_v_to_v_map;
    for(int v : vs) {
        v_to_in_v_map[v] = in_v_to_v_map.size();
        in_v_to_v_map.push_back(v);
    }

    int n = in_v_to_v_map.size();

    // adjust the size of _DynamicBitset
    vector<_DynamicBitset> adj;
    for(int i = 0; i < n; ++i) {
        adj.push_back(_DynamicBitset(n));
    }

    for(int i = 0; i < n; ++i) {
        for(int j = i+1; j < n; ++j) {
            int u = in_v_to_v_map[i];
            int v = in_v_to_v_map[j];
            int e_ij = wg.get_edge_weight(u, v);
            if(e_ij > 0) {
                adj[i].set_bit(j);
                adj[j].set_bit(i);
            }
        }
    }

    for(int i = 0; i < n; ++i) {
        for(int j = i+1; j < n; ++j) {
            int v_i = in_v_to_v_map[i];
            int v_j = in_v_to_v_map[j];
            int e_ij = wg.get_edge_weight(v_i, v_j);
            if(e_ij >= 0) continue;

            _DynamicBitset x(adj[i]._and(adj[j]));
            for(int k : x.gather_ones()) {
                int v_k = in_v_to_v_map[k];

                int e_ik = wg.get_edge_weight(v_i, v_k);
                int e_jk = wg.get_edge_weight(v_j, v_k);

                assert(v_i!=v_j);
                assert(v_i!=v_k);
                assert(v_j!=v_k);
                assert(e_ij < 0);
                assert(e_ik > 0);
                assert(e_jk > 0);

                int lb_cost = std::min(-e_ij, std::min(e_ik, e_jk));
                conflict_triples.push_back({lb_cost, {v_i, v_k, v_j}});
            }
        }
    }

    return conflict_triples;
}

vector<ConflictTriple> get_all_conflict_triples_old(const WeightedGraph & wg) {
    vector<std::pair<int, std::tuple<int,int,int>>> conflict_triples;
    const auto & vs = wg.all_vertices();
    // looking for conflict triple v_i,v_k,v_j => {v_i, v_j} is the non edge
    for(size_t i = 0; i < vs.size(); ++i) {
        for(size_t j = i+1; j < vs.size(); ++j) {
            int e_ij = wg.get_edge_weight(vs[i], vs[j]);
            if(e_ij >= 0) continue;
            int v_i = vs[i];
            int v_j = vs[j];

            for(size_t k = 0; k < vs.size(); ++k) {
                if(i==k || j==k) continue;
                int e_ik = wg.get_edge_weight(vs[i], vs[k]);
                int e_jk = wg.get_edge_weight(vs[j], vs[k]);

                if(e_ik <= 0 || e_jk <= 0) continue;

                int v_k = vs[k];

                assert(e_ij < 0);
                assert(e_ik > 0);
                assert(e_jk > 0);
                assert(v_i!=v_j);
                assert(v_i!=v_k);
                assert(v_k!=v_j);

                int lb_cost = std::min(-e_ij, std::min(e_ik, e_jk));
                conflict_triples.push_back({lb_cost, {v_i, v_k, v_j}});
            }
        }
    }
    return conflict_triples;
}

vector<set<int>> construct_conflict_triples_graph_adj(const vector<ConflictTriple> & conflict_triples) {
    std::map<std::pair<int,int>, vector<int>> edge_to_ct_idx;

    for(int ct_idx = 0; ct_idx < (int)conflict_triples.size(); ++ct_idx) {
        const auto & ct = conflict_triples[ct_idx];
        int v_i = std::get<0>(ct.second);
        int v_k = std::get<1>(ct.second);
        int v_j = std::get<2>(ct.second);
        edge_to_ct_idx[{std::min(v_i, v_j), std::max(v_i, v_j)}].push_back(ct_idx);
        edge_to_ct_idx[{std::min(v_i, v_k), std::max(v_i, v_k)}].push_back(ct_idx);
        edge_to_ct_idx[{std::min(v_j, v_k), std::max(v_j, v_k)}].push_back(ct_idx);
    }

    // conflict graph size
    int ct_n = conflict_triples.size();
    vector<set<int>> ct_adj(ct_n);
    for(const auto & x : edge_to_ct_idx) {
        const auto & ct_idxs = x.second;
        for(int i = 0; i < (int)ct_idxs.size(); ++i) {
            for(int j = i+1; j < (int)ct_idxs.size(); ++j) {
                int ct_i = ct_idxs[i];
                int ct_j = ct_idxs[j];
                ct_adj[ct_i].insert(ct_j);
                ct_adj[ct_j].insert(ct_i);
            }
        }
    }

    return ct_adj;
}

struct ConflictTriplesGraph {
    const vector<ConflictTriple> & cts;
    vector<vector<int>> edge_idx_to_ct_idx;
    vector<bool> edge_idx_valid;
    vector<int> edge_idxs;
    int ct_n;
    vector<bool> ct_valid;

    ConflictTriplesGraph(const vector<ConflictTriple> & cts) : cts(cts), ct_n(cts.size()), ct_valid(ct_n, true) {
        int max_v = 0;

        for(int ct_idx = 0; ct_idx < (int)cts.size(); ++ct_idx) {
            const auto & ct = cts[ct_idx];
            int v_i = std::get<0>(ct.second);
            int v_k = std::get<1>(ct.second);
            int v_j = std::get<2>(ct.second);
            if(v_i >= max_v) max_v = v_i + 1;
            if(v_k >= max_v) max_v = v_k + 1;
            if(v_j >= max_v) max_v = v_j + 1;
        }
        edge_idx_to_ct_idx.resize(max_v*(max_v+1)/2);
        edge_idx_valid.resize(max_v*(max_v+1)/2);
        edge_idx_valid.clear();

        // Timer _lb_timer3_ct;
        // _lb_timer3_ct.start();

        for(int ct_idx = 0; ct_idx < (int)cts.size(); ++ct_idx) {
            const auto & ct = cts[ct_idx];
            int v_i = std::get<0>(ct.second);
            int v_k = std::get<1>(ct.second);
            int v_j = std::get<2>(ct.second);

            int e_ij = get_edge_idx(v_i, v_j);
            int e_ik = get_edge_idx(v_i, v_k);
            int e_jk = get_edge_idx(v_j, v_k);

            edge_idx_to_ct_idx[e_ij].push_back(ct_idx);
            edge_idx_to_ct_idx[e_ik].push_back(ct_idx);
            edge_idx_to_ct_idx[e_jk].push_back(ct_idx);

            if(edge_idx_valid[e_ij] == 0){
                edge_idx_valid[e_ij] = 1;
                edge_idxs.push_back(e_ij);
            }
            if(edge_idx_valid[e_ik] == 0){
                edge_idx_valid[e_ik] = 1;
                edge_idxs.push_back(e_ik);
            }
            if(edge_idx_valid[e_jk] == 0){
                edge_idx_valid[e_jk] = 1;
                edge_idxs.push_back(e_jk);
            }

        }

        // _lb_timer3 += _lb_timer3_ct.stop();
    }

    int get_edge_idx(int u, int v) const {
        if(u < v) return v * (v-1) / 2 + u;
        return u * (u-1) / 2 + v;
    }

    set<int> get_ct_adj(int ct_idx) const {
        const auto & ct = cts[ct_idx];
        int v_i = std::get<0>(ct.second);
        int v_k = std::get<1>(ct.second);
        int v_j = std::get<2>(ct.second);
        int edge_idxs[] = {get_edge_idx(v_i, v_j), get_edge_idx(v_i, v_k), get_edge_idx(v_j, v_k)};

        set<int> ct_adj;

        for(int i = 0; i < 3; ++i) {
            int edge_idx = edge_idxs[i];
            for(int n_ct_idx : edge_idx_to_ct_idx.at(edge_idx)) {
                if(n_ct_idx == ct_idx) continue;
                if(ct_valid[n_ct_idx] == false) continue;
                ct_adj.insert(n_ct_idx);
            }
        }

        return ct_adj;
    }

    void delete_ct(int ct_idx) {
        ct_valid[ct_idx] = false;
    }

    bool ct_exists(int ct_idx) const {
        return ct_valid[ct_idx];
    }

};


// https://core.ac.uk/download/pdf/82558426.pdf
vector<ConflictTriple> select_greedy_disjoint_conflict_triples_gwmax(const WeightedGraph & wg) {
    // phase 1 - compute all conflict_triples together with their lb_cost
    vector<std::pair<int, std::tuple<int,int,int>>> cts = get_all_conflict_triples(wg);
    vector<set<int>> ct_adj = construct_conflict_triples_graph_adj(cts);
    int ct_n = ct_adj.size();

    vector<std::pair<int, std::tuple<int,int,int>>> selected_cts;

    auto cost_compute = [](int deg, int ct_cost) {
        if(deg==0) return -1;
        int p = (int)((1.*ct_cost/(deg*(deg+1)))*100000000);
        return p;
    };

    map<int, set<int>> prio_q;
    for(int i = 0; i < ct_n; ++i) {
        prio_q[cost_compute(ct_adj[i].size(), cts[i].first)].insert(i);
    }

    int ct_edge_cnt = 0;
    for(int i = 0; i < ct_n; ++i) {
        ct_edge_cnt += ct_adj[i].size();
    }
    ct_edge_cnt /= 2;


    vector<bool> ct_closed(ct_n, false);
    vector<bool> ct_del(ct_n, false);
    while(true) {
        if(ct_edge_cnt==0)break;
        if(prio_q.size()==0)break;
        auto & prio_q_item = *prio_q.begin();
        if(prio_q_item.second.size() == 0) {
            prio_q.erase(prio_q_item.first);
            continue;
        }
        int v = *prio_q_item.second.begin();
        prio_q_item.second.erase(v);
        // std::cerr<<"=> "<<prio_q_item.first<<" == "<<ct_adj[v].size()<<std::endl;
        if(ct_closed[v]) continue;
        if(prio_q_item.first < cost_compute(ct_adj[v].size(), cts[v].first)) continue;

        ct_closed[v] = true;
        if(ct_adj[v].size()==0) continue;
        ct_del[v] = true;

        for(int u : ct_adj[v]) {
            prio_q[cost_compute(ct_adj[u].size(), cts[u].first)].erase(u);
            ct_adj[u].erase(v);
            prio_q[cost_compute(ct_adj[u].size(), cts[u].first)].insert(u);
        }
        ct_edge_cnt -= ct_adj[v].size();
    }

    for(int i = 0; i < ct_n; ++i) {
        if(ct_del[i]==false) {
            selected_cts.push_back(cts[i]);
        }
    }

    return selected_cts;
}


// https://fpt.akt.tu-berlin.de/publications/clustEditImpl_LION15.pdf
vector<ConflictTriple> select_greedy_disjoint_conflict_triples_gwmin(const WeightedGraph & wg, const vector<ConflictTriple> & cts) {
    // phase 1 - compute all conflict_triples together with their lb_cost
    // vector<ConflictTriple> cts = get_all_conflict_triples(wg);
    vector<set<int>> ct_adj = construct_conflict_triples_graph_adj(cts);
    int ct_n = ct_adj.size();

    vector<ConflictTriple> selected_cts;

    auto cost_compute = [](int deg, int ct_cost) {
        return -(int)(((1.*ct_cost) / (deg + 1)) * 100000);
    };

    map<int, set<int>> prio_q;
    for(int i = 0; i < ct_n; ++i) {
        prio_q[cost_compute(ct_adj[i].size(), cts[i].first)].insert(i);
    }

    vector<bool> ct_closed(ct_n, false);

    while(true) {
        if(prio_q.size()==0)break;
        auto & prio_q_item = *prio_q.begin();
        if(prio_q_item.second.size() == 0) {
            prio_q.erase(prio_q_item.first);
            continue;
        }
        int v = *prio_q_item.second.begin();
        prio_q_item.second.erase(v);
        if(ct_closed[v]) continue;

        ct_closed[v] = true;
        for(int u : ct_adj[v]) {
            ct_closed[u] = true;
            ct_adj[u].erase(v);
        }

        for(int u : ct_adj[v]) {
            for(int n_u : ct_adj[u]) {
                prio_q[cost_compute(ct_adj[n_u].size(), cts[n_u].first)].erase(n_u);
                ct_adj[n_u].erase(u);
                ct_adj[n_u].erase(v);
                prio_q[cost_compute(ct_adj[n_u].size(), cts[n_u].first)].insert(n_u);
            }
        }
        selected_cts.push_back(cts[v]);
    }

    return selected_cts;
}

int _get_inner_rep_edge_idx(int in_u, int in_v) {
    assert(in_u!=in_v);
    if(in_u < in_v) return in_v * (in_v-1) / 2 + in_u;
    return in_u * (in_u-1) / 2 + in_v;
}

vector<ConflictTriple> select_greedy_disjoint_conflict_triples_gwmin_lowmem(const WeightedGraph & wg, const vector<ConflictTriple> & cts) {
    // phase 1 - compute all conflict_triples together with their lb_cost


    // time 20%
    // vector<ConflictTriple> cts = get_all_conflict_triples(wg);

    // time 40%
    // Timer _lb_timer2_ct;
    // _lb_timer2_ct.start();

    ConflictTriplesGraph ctg(cts);

    // _lb_timer2 += _lb_timer2_ct.stop();

    // time 0%
    vector<int> ct_degs(cts.size(), 0);
    for(int edge_idx : ctg.edge_idxs) {
        for(int v : ctg.edge_idx_to_ct_idx[edge_idx]) {
            ct_degs[v] += ctg.edge_idx_to_ct_idx[edge_idx].size() - 1;
        }
    }

    // time 20%
    vector<pair<int, int>> ct_prios;
    for(int v = 0; v < (int)cts.size(); ++v) {
        int ct_cost = cts[v].first;
        int ct_deg = ct_degs[v];
        int ct_prio = -(int)(((1.*ct_cost) / (ct_deg + 1)) * 100000);
        ct_prios.push_back({ct_prio, v});
    }
    sort(ct_prios.begin(), ct_prios.end());


    // time 10%
    vector<ConflictTriple> selected_cts;

    int max_v = 0;
    for(const auto & ct : cts) {
        int in_v_i = std::get<0>(ct.second);
        int in_v_k = std::get<1>(ct.second);
        int in_v_j = std::get<2>(ct.second);

        if(in_v_i >= max_v) max_v = in_v_i + 1;
        if(in_v_k >= max_v) max_v = in_v_k + 1;
        if(in_v_j >= max_v) max_v = in_v_j + 1;
    }

    vector<bool> ct_used_edges(max_v*(max_v+1)/2);
    for(auto p : ct_prios) {
        const auto & ct = cts[p.second];
        int in_v_i = std::get<0>(ct.second);
        int in_v_k = std::get<1>(ct.second);
        int in_v_j = std::get<2>(ct.second);

        int in_e_ij = _get_inner_rep_edge_idx(in_v_i, in_v_j);
        int in_e_ik = _get_inner_rep_edge_idx(in_v_i, in_v_k);
        int in_e_jk = _get_inner_rep_edge_idx(in_v_j, in_v_k);

        if(
            ct_used_edges[in_e_ij]||
            ct_used_edges[in_e_ik]||
            ct_used_edges[in_e_jk]
            ) continue;

        ct_used_edges[in_e_ij]=true;
        ct_used_edges[in_e_ik]=true;
        ct_used_edges[in_e_jk]=true;

        selected_cts.push_back(ct);
    }


    // std::cerr<<"END"<<std::endl;
    return selected_cts;
}


vector<ConflictTriple> select_greedy_disjoint_conflict_triples_gwmin2(const WeightedGraph & wg) {
    // phase 1 - compute all conflict_triples together with their lb_cost
    vector<ConflictTriple> cts = get_all_conflict_triples(wg);
    vector<set<int>> ct_adj = construct_conflict_triples_graph_adj(cts);
    int ct_n = ct_adj.size();

    vector<ConflictTriple> selected_cts;

    auto cost_compute = [&cts, &ct_adj](int deg, int ct_cost, int v) {
        (void) deg;
        int p = ct_cost;
        int s = ct_cost;
        for(int u : ct_adj[v]) s += cts[u].first;
        int r = (int)((1.*p / s) * 10000000);
        return -r;
    };

    map<int, set<int>> prio_q;
    for(int i = 0; i < ct_n; ++i) {
        prio_q[cost_compute(ct_adj[i].size(), cts[i].first, i)].insert(i);
    }

    vector<bool> ct_closed(ct_n, false);

    while(true) {
        if(prio_q.size()==0)break;
        auto & prio_q_item = *prio_q.begin();
        if(prio_q_item.second.size() == 0) {
            prio_q.erase(prio_q_item.first);
            continue;
        }
        int v = *prio_q_item.second.begin();
        prio_q_item.second.erase(v);
        if(ct_closed[v]) continue;

        ct_closed[v] = true;
        for(int u : ct_adj[v]) {
            ct_closed[u] = true;
            ct_adj[u].erase(v);
        }

        for(int u : ct_adj[v]) {
            for(int n_u : ct_adj[u]) {
                prio_q[cost_compute(ct_adj[n_u].size(), cts[n_u].first, n_u)].erase(n_u);
                ct_adj[n_u].erase(u);
                ct_adj[n_u].erase(v);
                prio_q[cost_compute(ct_adj[n_u].size(), cts[n_u].first, n_u)].insert(n_u);
            }
        }
        selected_cts.push_back(cts[v]);
    }

    return selected_cts;
}

// https://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.102.4678&rep=rep1&type=pdf
vector<ConflictTriple> select_greedy_disjoint_conflict_triples_wg_deg(const WeightedGraph & wg) {
    // phase 1 - compute all conflict_triples together with their lb_cost
    vector<ConflictTriple> cts = get_all_conflict_triples(wg);
    vector<set<int>> ct_adj = construct_conflict_triples_graph_adj(cts);
    int ct_n = ct_adj.size();

    vector<ConflictTriple> selected_cts;

    auto cost_compute = [&cts, &ct_adj](int deg, int ct_cost, int v) {
        (void) deg;
        int p = ct_cost;
        int s = 0;
        for(int u : ct_adj[v]) s += cts[u].first;
        int r = (int)((s / p) * 10000000);
        return r;
    };

    map<int, set<int>> prio_q;
    for(int i = 0; i < ct_n; ++i) {
        prio_q[cost_compute(ct_adj[i].size(), cts[i].first, i)].insert(i);
    }

    vector<bool> ct_closed(ct_n, false);

    while(true) {
        if(prio_q.size()==0)break;
        auto & prio_q_item = *prio_q.begin();
        if(prio_q_item.second.size() == 0) {
            prio_q.erase(prio_q_item.first);
            continue;
        }
        int v = *prio_q_item.second.begin();
        prio_q_item.second.erase(v);
        if(ct_closed[v]) continue;

        ct_closed[v] = true;
        for(int u : ct_adj[v]) {
            ct_closed[u] = true;
            ct_adj[u].erase(v);
        }

        for(int u : ct_adj[v]) {
            for(int n_u : ct_adj[u]) {
                prio_q[cost_compute(ct_adj[n_u].size(), cts[n_u].first, n_u)].erase(n_u);
                ct_adj[n_u].erase(u);
                ct_adj[n_u].erase(v);
                prio_q[cost_compute(ct_adj[n_u].size(), cts[n_u].first, n_u)].insert(n_u);
            }
        }
        selected_cts.push_back(cts[v]);
    }

    return selected_cts;
}




vector<ConflictTriple> select_greedy_disjoint_conflict_triples_greedy_sort(const WeightedGraph & wg) {
    // phase 1 - compute all conflict_triples together with their lb_cost
    vector<ConflictTriple> cts = get_all_conflict_triples(wg);

    // phase 2 - sort conflict_triples by lb_cost, we take the largest first
    std::sort(cts.begin(), cts.end(), [](const auto & a, const auto & b){
        if(a.first==b.first) {return a.second < b.second;}
        return a.first > b.first;
    });

    vector<ConflictTriple> selected_cts;

    std::unordered_set<int> ct_used_edges;
    for(const auto & ct : cts) {
        int in_v_i = std::get<0>(ct.second);
        int in_v_k = std::get<1>(ct.second);
        int in_v_j = std::get<2>(ct.second);

        int in_e_ij = _get_inner_rep_edge_idx(in_v_i, in_v_j);
        int in_e_ik = _get_inner_rep_edge_idx(in_v_i, in_v_k);
        int in_e_jk = _get_inner_rep_edge_idx(in_v_j, in_v_k);

        if(
            ct_used_edges.count(in_e_ij)||
            ct_used_edges.count(in_e_ik)||
            ct_used_edges.count(in_e_jk)
            ) continue;

        ct_used_edges.insert(in_e_ij);
        ct_used_edges.insert(in_e_ik);
        ct_used_edges.insert(in_e_jk);

        selected_cts.push_back(ct);
    }

    return selected_cts;
}


int compute_lb_from_conflict_triples(const vector<ConflictTriple> & cts) {
    int lb = 0;
    for(const auto & ct : cts) {
        lb += ct.first;
    }
    return lb;
}


vector<ConflictTriple> meta_select_greedy_disjoint_conflict_triples(const WeightedGraph & wg) {
    vector<ConflictTriple> cts = get_all_conflict_triples(wg);

    // const auto & cts_gwmax = select_greedy_disjoint_conflict_triples_gwmax(wg);
    int lb_cts_gwmax = 0;
    // int lb_cts_gwmax = compute_lb_from_conflict_triples(cts_gwmax);

    vector<ConflictTriple> cts_gwmin = select_greedy_disjoint_conflict_triples_gwmin_lowmem(wg, cts);
    // int lb_cts_gwmin = 0;
    int lb_cts_gwmin = compute_lb_from_conflict_triples(cts_gwmin);

    #ifdef MAIN_EXACT
        const int CTS_SIZE_THRESHOLD = 0;
    #else
        const int CTS_SIZE_THRESHOLD = 0;
    #endif

    if(CTS_SIZE_THRESHOLD > 0 && cts.size() < CTS_SIZE_THRESHOLD) {
        vector<ConflictTriple> cts_gwmin_extra = select_greedy_disjoint_conflict_triples_gwmin(wg, cts);
        int lb_cts_gwmin_extra = compute_lb_from_conflict_triples(cts_gwmin_extra);
        if(lb_cts_gwmin_extra > lb_cts_gwmin) {
            std::swap(cts_gwmin, cts_gwmin_extra);
            std::swap(lb_cts_gwmin_extra, lb_cts_gwmin);
        }
    }

    // const auto & cts_gwmin2 = select_greedy_disjoint_conflict_triples_gwmin2(wg);
    int lb_cts_gwmin2 = 0;
    // int lb_cts_gwmin2 = compute_lb_from_conflict_triples(cts_gwmin2);

    // const auto & cts_gs = select_greedy_disjoint_conflict_triples_greedy_sort(wg);
    int lb_cts_gs = 0;
    // int lb_cts_gs = compute_lb_from_conflict_triples(cts_gs);

    // const auto & cts_wg_deg = select_greedy_disjoint_conflict_triples_wg_deg(wg);
    int lb_cts_wg_deg = 0;
    // int lb_cts_wg_deg = compute_lb_from_conflict_triples(cts_wg_deg);

    int lb = 0;
    if(lb_cts_gwmax>lb)lb=lb_cts_gwmax;
    if(lb_cts_gwmin>lb)lb=lb_cts_gwmin;
    if(lb_cts_gwmin2>lb)lb=lb_cts_gwmin2;
    if(lb_cts_gs>lb)lb=lb_cts_gs;
    if(lb_cts_wg_deg>lb)lb=lb_cts_wg_deg;

    if(lb_cts_gwmin != lb) {
        std::cerr<<
        "lb_cts_gwmax="<<lb_cts_gwmax<<", "
        "lb_cts_gwmin="<<lb_cts_gwmin<<", "
        "lb_cts_gwmin2="<<lb_cts_gwmin2<<", "
        "lb_cts_gs="<<lb_cts_gs<<", "
        "lb_cts_wg_deg="<<lb_cts_wg_deg<<", "
        <<std::endl;
    }

    // if(lb_cts_gwmax == lb) return cts_gwmax;
    if(lb_cts_gwmin == lb) return cts_gwmin;
    // if(lb_cts_gwmin2 == lb) return cts_gwmin2;
    // if(lb_cts_gs == lb) return cts_gs;
    // if(lb_cts_wg_deg == lb) return cts_wg_deg;
    assert(0);
}


int greedy_disjoint_conflict_triples_v03(const WeightedGraph & wg) {
    int lb = 0;
    for(const auto & ct : meta_select_greedy_disjoint_conflict_triples(wg)) {
        lb += ct.first;
    }
    return lb;
}

int greedy_disjoint_conflict_triples_v02(const WeightedGraph & wg) {
    return GreedyDisjointConflictTriplesLowerBound(wg).get_lower_bound();
}

GreedyDisjointConflictTriplesLowerBound::~GreedyDisjointConflictTriplesLowerBound() {
    // std::cerr<<"_lb_timer1="<<_lb_timer1<<std::endl;
    // std::cerr<<"_lb_timer2="<<_lb_timer2<<std::endl;
    // std::cerr<<"_lb_timer3="<<_lb_timer3<<std::endl;
}

GreedyDisjointConflictTriplesLowerBound::GreedyDisjointConflictTriplesLowerBound(const WeightedGraph & wg) : wg(wg) {
    // ContextTimer _lb_timer1_ct(_lb_timer1);

    vector<int> vs = wg.all_vertices();
    int v_max = -1;
    for(int v : vs) {if(v>=v_max)v_max=v+1;}
    v_to_in_v_map.resize(v_max, -1);
    int n = 0;
    for(int v : vs){v_to_in_v_map[v] = n++;}
    assert(n==(int)vs.size());
    in_v_to_v_map.resize(n, 0);
    for(int v : vs){in_v_to_v_map[v_to_in_v_map[v]] = v;}

    vertex_conflict_triple_lb.resize(n, 0);
    edge_conflict_triple_lb.resize(n*(n-1)/2, 0);
    edge_conflict_triple.resize(n*(n-1)/2, {-1,-1,-1});
    conflict_triple_lb = 0;


    // phase 1 - compute all conflict_triples together with their lb_cost
    vector<std::pair<int, std::tuple<int,int,int>>> conflict_triples;
    for(const auto & ct : meta_select_greedy_disjoint_conflict_triples(wg)) {
        conflict_triples.push_back({
            ct.first,
            {
                v_to_in_v_map[std::get<0>(ct.second)],
                v_to_in_v_map[std::get<1>(ct.second)],
                v_to_in_v_map[std::get<2>(ct.second)],
            }
        });
    }



    vector<bool> conflict_triple_used_edges(n*(n-1)/2, false);

    for(const auto & ct : conflict_triples) {
        int in_v_i = std::get<0>(ct.second);
        int in_v_k = std::get<1>(ct.second);
        int in_v_j = std::get<2>(ct.second);

        int in_e_ij = _get_inner_rep_edge_idx(in_v_i, in_v_j);
        int in_e_ik = _get_inner_rep_edge_idx(in_v_i, in_v_k);
        int in_e_jk = _get_inner_rep_edge_idx(in_v_j, in_v_k);

        if(
            conflict_triple_used_edges[in_e_ij]||
            conflict_triple_used_edges[in_e_ik]||
            conflict_triple_used_edges[in_e_jk]
            ) continue;

        conflict_triple_used_edges[in_e_ij] = true;
        conflict_triple_used_edges[in_e_ik] = true;
        conflict_triple_used_edges[in_e_jk] = true;

        conflict_triple_lb += ct.first;
        vertex_conflict_triple_lb[in_v_i] += ct.first;
        vertex_conflict_triple_lb[in_v_k] += ct.first;
        vertex_conflict_triple_lb[in_v_j] += ct.first;

        edge_conflict_triple_lb[in_e_ij] = ct.first;
        edge_conflict_triple[in_e_ij] = ct.second;
        edge_conflict_triple_lb[in_e_ik] = ct.first;
        edge_conflict_triple[in_e_ik] = ct.second;
        edge_conflict_triple_lb[in_e_jk] = ct.first;
        edge_conflict_triple[in_e_jk] = ct.second;
    }

}


int GreedyDisjointConflictTriplesLowerBound::_get_inner_rep_edge_idx(int in_u, int in_v) const {
    assert(in_u!=in_v);
    if(in_u < in_v) std::swap(in_u, in_v);
    return in_u * (in_u-1) / 2 + in_v;
}

int GreedyDisjointConflictTriplesLowerBound::get_lower_bound() const {
    return conflict_triple_lb;
}

int GreedyDisjointConflictTriplesLowerBound::get_masked_lower_bound(int u, int v) const {
    int in_u = v_to_in_v_map[u];
    int in_v = v_to_in_v_map[v];
    assert(in_u != -1);
    assert(in_v != -1);
    assert(in_u!=in_v);
    int e_uv = _get_inner_rep_edge_idx(in_u, in_v);

    return conflict_triple_lb - vertex_conflict_triple_lb[in_u] - vertex_conflict_triple_lb[in_v] + edge_conflict_triple_lb[e_uv];
}

void GreedyDisjointConflictTriplesLowerBound::update_lower_bound_edge_forbidden(int u, int v) {
    int in_u = v_to_in_v_map[u];
    int in_v = v_to_in_v_map[v];

    int e_uv = _get_inner_rep_edge_idx(in_u, in_v);

    auto ct = edge_conflict_triple[e_uv];

    // recall that v_i, v_j is the non edge
    int in_v_i = std::get<0>(ct);
    int in_v_k = std::get<1>(ct);
    int in_v_j = std::get<2>(ct);

    // no conflict triple for edge u,v therefore we do nothing
    if(in_v_i == -1) {return;}

    int v_i = in_v_to_v_map[in_v_i];
    int v_k = in_v_to_v_map[in_v_k];
    int v_j = in_v_to_v_map[in_v_j];

    int e_ij = wg.get_edge_weight(v_i, v_j);
    int e_ik = wg.get_edge_weight(v_i, v_k);
    int e_jk = wg.get_edge_weight(v_j, v_k);

    assert(e_ij < 0);
    assert(e_ik > 0);
    assert(e_jk > 0);
    assert(v_i!=v_j);
    assert(v_i!=v_k);
    assert(v_k!=v_j);

    int in_e_ij = _get_inner_rep_edge_idx(in_v_i, in_v_j);
    int in_e_ik = _get_inner_rep_edge_idx(in_v_i, in_v_k);
    int in_e_jk = _get_inner_rep_edge_idx(in_v_j, in_v_k);

    // if u,v is the non edge
    if(in_u != in_v_k && in_v != in_v_k) {
        int old_lb_cost = std::min(-e_ij, std::min(e_ik, e_jk));
        int new_lb_cost = std::min(e_ik, e_jk);

        conflict_triple_lb += new_lb_cost - old_lb_cost;
        vertex_conflict_triple_lb[in_v_i] += new_lb_cost - old_lb_cost;
        vertex_conflict_triple_lb[in_v_k] += new_lb_cost - old_lb_cost;
        vertex_conflict_triple_lb[in_v_j] += new_lb_cost - old_lb_cost;

        edge_conflict_triple_lb[in_e_ij] = new_lb_cost;
        edge_conflict_triple_lb[in_e_ik] = new_lb_cost;
        edge_conflict_triple_lb[in_e_jk] = new_lb_cost;
    }
    // else we are setting an edge to forbidden, which removes the conflict, so we delete the conflict
    else {
        int old_lb_cost = std::min(-e_ij, std::min(e_ik, e_jk));
        conflict_triple_lb -= old_lb_cost;
        vertex_conflict_triple_lb[in_v_i] -= old_lb_cost;
        vertex_conflict_triple_lb[in_v_k] -= old_lb_cost;
        vertex_conflict_triple_lb[in_v_j] -= old_lb_cost;

        edge_conflict_triple_lb[in_e_ij] = 0;
        edge_conflict_triple_lb[in_e_ik] = 0;
        edge_conflict_triple_lb[in_e_jk] = 0;

        edge_conflict_triple[in_e_ij] = {-1,-1,-1};
        edge_conflict_triple[in_e_ik] = {-1,-1,-1};
        edge_conflict_triple[in_e_jk] = {-1,-1,-1};
    }

}

void GreedyDisjointConflictTriplesLowerBound::update_lower_bound_edge_merge_u_into_v(int u, int v) {
    // for simplicity, we will delete all conflict triples that use vertices u, v
    // this is mighty suboptimal, but easy to implement

    _delete_conflict_triples_for_vertex(u);
    _delete_conflict_triples_for_vertex(v);

}

void GreedyDisjointConflictTriplesLowerBound::_delete_conflict_triple_for_edge(int u, int v) {
    int in_u = v_to_in_v_map[u];
    int in_v = v_to_in_v_map[v];

    int e_uv = _get_inner_rep_edge_idx(in_u, in_v);

    auto ct = edge_conflict_triple[e_uv];

    // recall that v_i, v_j is the non edge
    int in_v_i = std::get<0>(ct);
    int in_v_k = std::get<1>(ct);
    int in_v_j = std::get<2>(ct);

    // no conflict triple for edge u,v therefore we do nothing
    if(in_v_i == -1) {return;}

    int v_i = in_v_to_v_map[in_v_i];
    int v_k = in_v_to_v_map[in_v_k];
    int v_j = in_v_to_v_map[in_v_j];

    int e_ij = wg.get_edge_weight(v_i, v_j);
    int e_ik = wg.get_edge_weight(v_i, v_k);
    int e_jk = wg.get_edge_weight(v_j, v_k);

    assert(e_ij < 0);
    assert(e_ik > 0);
    assert(e_jk > 0);
    assert(v_i!=v_j);
    assert(v_i!=v_k);
    assert(v_k!=v_j);

    int in_e_ij = _get_inner_rep_edge_idx(in_v_i, in_v_j);
    int in_e_ik = _get_inner_rep_edge_idx(in_v_i, in_v_k);
    int in_e_jk = _get_inner_rep_edge_idx(in_v_j, in_v_k);

    int lb_cost = std::min(-e_ij, std::min(e_ik, e_jk));

    conflict_triple_lb -= lb_cost;
    vertex_conflict_triple_lb[in_v_i] -= lb_cost;
    vertex_conflict_triple_lb[in_v_k] -= lb_cost;
    vertex_conflict_triple_lb[in_v_j] -= lb_cost;

    edge_conflict_triple_lb[in_e_ij] = 0;
    edge_conflict_triple_lb[in_e_ik] = 0;
    edge_conflict_triple_lb[in_e_jk] = 0;

    edge_conflict_triple[in_e_ij] = {-1,-1,-1};
    edge_conflict_triple[in_e_ik] = {-1,-1,-1};
    edge_conflict_triple[in_e_jk] = {-1,-1,-1};
}

void GreedyDisjointConflictTriplesLowerBound::_delete_conflict_triples_for_vertex(int v) {
    const auto & vs = wg.all_vertices();
    for(int w : vs) {
        if(w==v) continue;
        _delete_conflict_triple_for_edge(w, v);
    }
}


