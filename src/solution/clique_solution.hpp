#pragma once

#include <vector>

#include "../solution.hpp"
#include "../graph/weighted_graph.hpp"
#include "../graph/neighbours_graph.hpp"

using std::vector;

/*!
 * Class for managing the solution -- a list of clique components.
 */
class CliqueSolution {
    public:

        CliqueSolution();
        CliqueSolution(const WeightedGraph &graph);
        CliqueSolution(vector<vector<int>> components);

        /*!
         * Put a previously non-existant vertex into a component which contains
         * a given vertex.
         */
        void add_vertex_to_component_of ( int new_vertex, int components_vertex );

        /*!
         * Put a previously non-existant vertex and create a new component for it.
         */
        void add_vertex_as_a_component ( int new_vertex );

        /*!
         * Rename vertices of all edges according to the mapping. Vertices which are
         * not mentioned in the mapping remain unchanged.
         */
        void map_vertices ( const map<int,int> & mapping );

        /*!
         * Rename vertices of all edges according to the mapping. Vertices which are
         * not mentioned in the mapping remain unchanged.
         */
        void map_vertices ( const vector<int> & mapping );

        /*!
         * Merges other solution into this.
         */
        void merge ( const CliqueSolution & other );

        /*!
         * Returns the whole solution.
         */
        Solution * get_solution ( const WeightedGraph * g ) const;

        Solution * get_solution ( const NeighboursGraph * g ) const;

        friend ostream& operator<<(ostream &os, const CliqueSolution &c);

        const vector<vector<int>> & _get_components() const;

        int component_of(int vertex) const;

        int get_cost( const WeightedGraph &) const;

        int vertex_count() const;

    private:
        vector<vector<int>> components;
        map<int,int> vertex_component;
};
