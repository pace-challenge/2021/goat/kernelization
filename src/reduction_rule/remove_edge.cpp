#include "remove_edge.hpp"

RemoveEdgeReduction::RemoveEdgeReduction ( int from, int to )
    : from ( from ), to ( to ) { }

void RemoveEdgeReduction::apply ( Graph *& graph, Bound & bound ) {
    graph->remove_edge( from, to );
    bound.decrease_by( 1 );
}

void RemoveEdgeReduction::reconstruct_solution ( CliqueSolution * solution ) const {
    (void) solution; // removing edge doesn't change the resulting components
}
