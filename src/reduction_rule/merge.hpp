#pragma once

#include "reduction_rule.hpp"
#include "../graph/weighted_graph.hpp"

/*!
 * Merge two vertices of the weighted graph by adding respective edge weights.
 */
class MergeReduction : public ReductionRule<WeightedGraph> {
    public:

        MergeReduction ( int what, int into );

        void apply ( WeightedGraph *& graph, Bound & bound ) ;

        void reconstruct_solution ( CliqueSolution * solution ) const ;

    private:
        int what, into; /*!< The edded edge endpoints */
};

