#include "flip_edge.hpp"

FlipEdgeReduction::FlipEdgeReduction ( int from, int to )
    : from ( from ), to ( to ) { }

void FlipEdgeReduction::apply ( WeightedGraph *& graph, Bound & bound ) {
    bound.decrease_by( abs(graph->get_edge_weight(from, to)) );
    graph->flip_edge( from, to );
}

void FlipEdgeReduction::reconstruct_solution ( CliqueSolution * solution ) const {
    (void) solution;
}
