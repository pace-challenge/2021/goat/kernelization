#include "remove_vertex.hpp"

RemoveVertexReduction::RemoveVertexReduction ( int v )
    : vertex ( v ) { }

void RemoveVertexReduction::apply ( WeightedGraph *& graph, Bound & bound ) {
    vector<int> N = graph->neighbors( this->vertex );
    for ( auto u : N ) assert( graph->get_edge_weight(this->vertex, u) <= 0 );
    graph->delete_vertex( this->vertex );
    (void) bound; // removing the vertex should be done when it is solved -- no edge changes
}

void RemoveVertexReduction::reconstruct_solution ( CliqueSolution * solution ) const {
    solution->add_vertex_as_a_component( this->vertex );
}
