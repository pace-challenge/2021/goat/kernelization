#pragma once

#include "reduction_rule.hpp"
#include "../graph/weighted_graph.hpp"

/*!
 * Make an edge between vertices (from, to) forbidden.
 * We have to pay if the edge has a positive weight.
 */
class ForbidEdgeReduction : public ReductionRule<WeightedGraph> {
    public:

        ForbidEdgeReduction ( int from, int to );

        void apply ( WeightedGraph *& graph, Bound & bound ) ;

        void reconstruct_solution ( CliqueSolution * solution ) const ;

    private:
        int from, to;
};

