#pragma once

#include "reduction_rule.hpp"
#include "../graph/graph.hpp"

/*!
 * Adds an edge in an attempt to find the solution.
 */
class AddEdgeReduction : public ReductionRule<Graph> {
    public:

        AddEdgeReduction ( int from, int to );

        void apply ( Graph *& graph, Bound & bound ) ;

        void reconstruct_solution ( CliqueSolution * solution ) const ;

    private:
        int from, to; /*!< The edded edge endpoints */
};

class AddWeightedEdgeReduction : public ReductionRule<WeightedGraph> {
    public:
        AddWeightedEdgeReduction ( int from, int to, int weight );

        void apply ( WeightedGraph *& graph, Bound & bound ) override;

        void reconstruct_solution ( CliqueSolution * solution ) const override;

    private:
        int from, to, weight, original_weight;
};
