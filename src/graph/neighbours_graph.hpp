#pragma once

#include <vector>
#include <list>
#include <ostream>

using std::vector;
using std::list;
using std::ostream;

/*!
 * Class representing single graph.
 */
class NeighboursGraph {
    public:
        /*!
         * Constructor.
         *
         * \param[in] n Number of vertices.
         */
        NeighboursGraph ( int n);

        /*!
         * Retrieves total number of vertices.
         */
        size_t get_vertices_count() const;

        /*!
         * Retrieves total number of edges.
         */
        size_t get_edges_count() const;

        /*!
         * Returns the neighborhood of the vertex v.
         */
        const vector<int> & get_adjacency_list ( int v ) const;


        /*!
         * Checks whether graph contains edge between vertices u and v.
            it is O(n)
         */
        bool contains_edge ( int u, int v ) const;

        /*!
         * Add {u,v} edge into the graph.
         */
        void add_edge ( int u, int v );

        /*!
         * Add {u,v} edge into the graph, 
         * the edge must ot be in the graph
         */
        void add_edge_unsafe ( int u, int v );

        /*!
         * Removes {u,v} edge from the graph.
         */
        void remove_edge ( int u, int v );

        /*!
         * Prints graph to a stream.
         */
        void print( ostream & out ) const;

    private:
        size_t n; /*!< Number of vertices of the graph. */
        size_t m; /*!< Number of edges of the graph. */
        vector<vector<int>> alist; /*!< Adjacency list representing the graph. */
};
