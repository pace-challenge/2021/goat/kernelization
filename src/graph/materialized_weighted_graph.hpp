#pragma once

#include "graph.hpp"
#include "weighted_graph.hpp"
#include "../lower_bounds/greedy_disjoint_conflict_triples.hpp"
#include "../utils/induced_costs.hpp"

#include <iostream>
#include <map>
#include <set>
#include <vector>
using std::map;
using std::ostream;
using std::set;
using std::vector;
using std::pair;


/*!
 *
 *
 */
class MaterializedWeightedGraph : public WeightedGraph {

public:
    virtual int get_edge_weight(int u, int v) const;
    virtual void set_edge_weight(int u, int v, int val);

    virtual void unsafe_set_edge_weight(int u, int v, int val);
    virtual void set_edge_forbidden(int u, int v);
    virtual void merge(int what, int into);

    virtual int add_vertex(int v);
    virtual void delete_vertex(int v);
    virtual bool vertex_exists(int v) const;

    virtual vector<int> all_vertices() const;
    virtual int size() const;

    virtual WeightedGraph* copy() const;
    virtual void print(ostream &os) const;
    virtual std::string to_json() const;

    virtual WeightedGraph* induced_subgraph(const vector<int> & chosen_vertices) const;

    /*!
     * creates a graph with n vertices and all edges set to val weight
     */
    MaterializedWeightedGraph(int n);
    MaterializedWeightedGraph(int n, int val);
    MaterializedWeightedGraph(const MaterializedWeightedGraph &wg);

    ~MaterializedWeightedGraph();

    virtual WeightedGraph* new_empty_graph() const;

    // int get_lower_bound() const;
    // int get_masked_lower_bound(int u, int v) const;

    int get_icp(int u, int v) const;
    int get_icf(int u, int v) const;

    void recompute_structures();
    void free_structures();

protected:
    int n;
    int _size;
    vector<int> edge_weights;
    vector<bool> vertices_validity_mask;

    bool structures_valid;

    // GreedyDisjointConflictTriplesLowerBound * lb_structures;
    InducedCosts * ic_structures;

    /*!
     * helper
     * returns the idx of the edge in the inner representation
     */
    int get_edge_idx(int u, int v) const;
    /*!
     * helper
     * extends the inner representation to accomodate one more vertex
     */
    void extend_representation();

    void _delete_vertex(int v);

};
