#include "old_materialized_graph.hpp"
#include <cassert>
#include <iomanip>


int OldMaterializedGraph::get_edge_idx(int u, int v) const {
    if(u < v) std::swap(u, v);
    return u * (u-1) / 2 + v;
}

/////////////////////////////////////////////////////////////////////////

int OldMaterializedGraph::get_edge_weight(int u, int v) const {
    assert(vertex_exists(u));
    assert(vertex_exists(v));
    assert(u!=v);
    return edge_weights[get_edge_idx(u, v)];
}

/////////////////////////////////////////////////////////////////////////

void OldMaterializedGraph::set_edge_weight(int u, int v, int val) {
    assert(vertex_exists(u));
    assert(vertex_exists(v));
    assert(u!=v);
    edge_weights[get_edge_idx(u, v)] = val;
}

/////////////////////////////////////////////////////////////////////////

vector<int> OldMaterializedGraph::all_vertices() const {
    vector<int> vs;
    for(int i = 0; i < n; ++i) {
        if(vertices_validity_mask[i]) vs.push_back(i);
    }
    return vs;
}

/////////////////////////////////////////////////////////////////////////

int OldMaterializedGraph::size() const {
    return _size;
}

/////////////////////////////////////////////////////////////////////////

OldMaterializedGraph::OldMaterializedGraph(const OldMaterializedGraph &wg)
    : n(wg.n), _size(wg._size), edge_weights(wg.edge_weights), vertices_validity_mask(wg.vertices_validity_mask) {}

/////////////////////////////////////////////////////////////////////////

WeightedGraph* OldMaterializedGraph::copy() const {
    return new OldMaterializedGraph(*this);
}

/////////////////////////////////////////////////////////////////////////

OldMaterializedGraph::OldMaterializedGraph(int n, int val)
    : n(n), _size(n), edge_weights(n*(n-1)/2, val), vertices_validity_mask(n, true) {}

OldMaterializedGraph::OldMaterializedGraph(int n)
    : OldMaterializedGraph(n, -1) {}

/////////////////////////////////////////////////////////////////////////

void OldMaterializedGraph::extend_representation()  {
    // add n edges
    for(int i = 0; i < n; ++i) edge_weights.push_back(-1);
    // then increment n and vertices_validity_mask
    n++;
    vertices_validity_mask.push_back(false);
}

/////////////////////////////////////////////////////////////////////////

int OldMaterializedGraph::add_vertex(int v) {
    assert(v>=0);
    while(v >= n) extend_representation();
    if(vertices_validity_mask[v] == true) return v;

    vertices_validity_mask[v] = true;
    _size++;
    for(int u = 0; u < n; ++u) {
        if(!vertices_validity_mask[u]) continue;
        if(u == v) continue;
        set_edge_weight(u, v, -1);
    }

    return v;
}

/////////////////////////////////////////////////////////////////////////

void OldMaterializedGraph::delete_vertex(int v) {
    if(vertices_validity_mask[v] == false) return;
    vertices_validity_mask[v] = false;
    _size--;
}

/////////////////////////////////////////////////////////////////////////

bool OldMaterializedGraph::vertex_exists(int u) const {
    if(u<0 || u>=n) return false;
    return vertices_validity_mask[u];
}

/////////////////////////////////////////////////////////////////////////

WeightedGraph* OldMaterializedGraph::new_empty_graph() const {
    return new OldMaterializedGraph(0, -1);
}

/////////////////////////////////////////////////////////////////////////

void OldMaterializedGraph::merge(int what, int into) {
    assert(vertex_exists(what));
    assert(vertex_exists(into));
    assert(what != into);
    for(int u = 0; u < n; ++u) {
        if(!vertices_validity_mask[u]) continue;
        if(u == what) continue;
        if(u == into) continue;

        int what_u = get_edge_weight(what, u);
        int into_u = get_edge_weight(into, u);
        int new_weight = what_u + into_u;

        // does not work with PERMANENT weights
        assert(what_u != PERMANENT_WEIGHT);
        assert(into_u != PERMANENT_WEIGHT);
        if(what_u == FORBIDDEN_WEIGHT) new_weight = FORBIDDEN_WEIGHT;
        if(into_u == FORBIDDEN_WEIGHT) new_weight = FORBIDDEN_WEIGHT;
        set_edge_weight(into, u, new_weight);
    }
    delete_vertex(what);
}


/////////////////////////////////////////////////////////////////////////

void OldMaterializedGraph::print(ostream &os) const {
    os << "vertices size: " << _size << std::endl;
    os << "vertices: ";
    for(int u = 0; u < n; ++u) {
        if(!vertices_validity_mask[u]) continue;
        os << " " << u;
    }
    os << std::endl;

    os << "edges: \n";
    for(int u = 0; u < n; ++u) {
        if(!vertices_validity_mask[u]) continue;
        for(int v = 0; v < u; ++v) {
            if(!vertices_validity_mask[v]) continue;
            os << std::setw(3) << u << "," << v << " -> " << get_edge_weight(u, v) << std::endl;
        }
    }
}

/////////////////////////////////////////////////////////////////////////

std::string OldMaterializedGraph::to_json() const {
    std::string j_vertices = "";
    for(int u = 0; u < n; ++u) {
        if(!vertices_validity_mask[u]) continue;

        if(j_vertices.size()) j_vertices+=",";
        j_vertices += std::to_string(u);
    }
    j_vertices = "[" + j_vertices + "]";

    std::string j_edges = "";
    for(int u = 0; u < n; ++u) {
        if(!vertices_validity_mask[u]) continue;
        for(int v = 0; v < u; ++v) {
            if(!vertices_validity_mask[v]) continue;

            if(j_edges.size()) j_edges+=",";
            j_edges += "[" + std::to_string(u) + "," + std::to_string(v) + "," + std::to_string(get_edge_weight(u, v)) + "]";
        }
    }
    j_edges = "[" + j_edges + "]";

    std::string j = std::string("{") + "\"vertices\":" + j_vertices + "," + "\"edges\":" + j_edges + "}";
    return j;
}
