#include "priority_strategy.hpp"

#include <functional>

using std::make_pair;

PriorityStrategy::PriorityStrategy(function<int(const Node *)> evaluate_node_fun)
    :seq_num(0),evaluate_node_fun(evaluate_node_fun){ }

PriorityStrategy::~PriorityStrategy(){}

Node *PriorityStrategy::next_node(){
    Node *best = nullptr;
    while(!priorities.empty()){
        auto biggest = priorities.end(); --biggest;
        Node *next = biggest->second;
        auto it = nodes.find(next);
        if(it != nodes.end()){
            best = next;
            break;
        }else{
            priorities.erase(biggest);
        }
    }
    return best;
}

void PriorityStrategy::created_node(Node *n){
    if(should_be_evaluated(n->get_state())) {
        int score = evaluate_node_fun(n);
        nodes.insert(n);
        priorities.insert(make_pair(make_pair(score,--seq_num), n));
    }
}

void PriorityStrategy::changed_node(Node *n){
    auto it = nodes.find(n);
    if(it != nodes.end() && !should_be_evaluated(n->get_state())) {
        nodes.erase(it);
    }
}

void PriorityStrategy::deleted_node(Node *n){
    auto it = nodes.find(n);
    if(it != nodes.end()){
        nodes.erase(it);
    }
}
