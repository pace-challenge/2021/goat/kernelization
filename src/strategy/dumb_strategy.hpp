#pragma once

#include "strategy.hpp"
#include "../node/node.hpp"

class DumbStrategy : public SelectNodeStrategy {
    public:
        DumbStrategy();
        virtual ~DumbStrategy();
        virtual Node* next_node();
        virtual void created_node(Node*);
        virtual void changed_node(Node*);
        virtual void deleted_node(Node*);

    private:
        set<Node*> nodes;
};
