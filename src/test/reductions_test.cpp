#include <algorithm>
#include <vector>

#include "../graph/weighted_graph.hpp"
#include "../graph/dumb_weighted_graph.hpp"
#include "../reduction_rule/merge.hpp"
#include "tester.hpp"

using std::vector;
using std::reverse;

Tester t;

WeightedGraph* get_compelte(int N){
    WeightedGraph *graph = new DumbWeightedGraph(N);
    for(int i=0; i<N; ++i){
        for(int j=i+1; j<N; ++j){
            graph->set_edge_weight(i,j,1);
        }
    }
    return graph;
}

CliqueSolution * test_reductions(WeightedGraph *graph, vector<ReductionRule<WeightedGraph>*> &reductions){
    Bound bound(0, 500);
    for(auto &r:reductions) r->apply(graph, bound);
    CliqueSolution *solution = new CliqueSolution(*graph);
    reverse(reductions.begin(),reductions.end());
    for(auto &r:reductions) r->reconstruct_solution(solution);
    reverse(reductions.begin(),reductions.end());
    return solution;
}

int remove_random(vector<int> &v){
    t.is_true(v.size() != 0);
    int idx = rand()%v.size();
    std::swap(v[idx],v[v.size()-1]);
    int res = v.back();
    v.pop_back();
    return res;
}

int main(void){
    t.check("random merge reductions");
    vector<WeightedGraph*> graphs;
    for(int i=5; i<80; ++i){
        WeightedGraph* graph = get_compelte(i);

        vector<ReductionRule<WeightedGraph>*> reductions;
        int r = rand() % (graph->size()-1);
        vector<int> v=graph->all_vertices();
        for(int i=0; i<r; ++i){
            int f = remove_random(v);
            int t = v[rand() % v.size()];
            reductions.push_back(new MergeReduction(f, t));
        }

        test_reductions(graph, reductions);
        /* std::cout << (solution->get_solution(graph)) << std::endl; */
        for(auto *r : reductions) delete r;
        delete graph;
    }
    t.ok();
}
