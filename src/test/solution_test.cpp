#include <cassert>

#include "../graph/weighted_graph.hpp"
#include "../graph/dumb_weighted_graph.hpp"
#include "../reduction_rule/merge.hpp"
#include "../graph/graph.hpp"
#include "../graph/conversion.hpp"
#include "../utils/get_components.hpp"
#include "tester.hpp"

using std::cout;
using std::endl;

int main(void){
    Tester t;
    t.check("former counterexample for clique-solution");
    // we have k4 minus edge
    Graph k4e = Graph(4, 0);
    k4e.add_edge(0,1);
    k4e.add_edge(0,2);
    k4e.add_edge(0,3);
    k4e.add_edge(1,3);
    k4e.add_edge(2,3);
    auto graphFactory = [](int SZ){ return new DumbWeightedGraph(SZ); };
    WeightedGraph * wk4e = convert_graph_to_weighted_graph(k4e, graphFactory);
    WeightedGraph * original = wk4e->copy();
    MergeReduction merge1(0,1);
    Bound bound(0, 50);
    merge1.apply(wk4e, bound);
    MergeReduction merge2(2,3);
    merge2.apply(wk4e, bound);
    CliqueSolution k4e_clique_solution(*wk4e);
    merge2.reconstruct_solution(&k4e_clique_solution);
    merge1.reconstruct_solution(&k4e_clique_solution);
    Solution *k4e_solution = k4e_clique_solution.get_solution(original);
    t.is_true(k4e_solution->weight() == 1);
    delete k4e_solution;
    delete original;
    delete wk4e;
    t.ok();

    {
        t.check("merging counterexample");
        DumbWeightedGraph disconnected = DumbWeightedGraph(4);
        disconnected.set_edge_weight(0,1,1);
        disconnected.set_edge_weight(2,3,1);
        auto comps = get_components(disconnected);
        vector<CliqueSolution> sols;
        for(auto c : comps){
            auto q = disconnected.induced_subgraph(c);
            sols.push_back(CliqueSolution(*q));
        }
        CliqueSolution solution;
        for(CliqueSolution &s : sols) solution.merge(s);

        solution.add_vertex_to_component_of(4, 0);
        solution.add_vertex_to_component_of(5, 2);

        DumbWeightedGraph final_graph = DumbWeightedGraph(6);
        final_graph.set_edge_weight(0,1,1);
        final_graph.set_edge_weight(0,4,1);
        final_graph.set_edge_weight(2,3,1);
        final_graph.set_edge_weight(2,5,1);
        Solution *s = solution.get_solution(&final_graph);
        t.is_true(s->weight() == 2);
        t.is_true(!(s->contains_edge(0, 1)));
        t.is_true(!(s->contains_edge(0, 2)));
        t.is_true(!(s->contains_edge(0, 3)));
        t.is_true(!(s->contains_edge(0, 5)));
        t.is_true(!(s->contains_edge(1, 2)));
        t.is_true(!(s->contains_edge(1, 3)));
        t.is_true( (s->contains_edge(1, 4))); // <-
        t.is_true(!(s->contains_edge(1, 5)));
        t.is_true(!(s->contains_edge(2, 3)));
        t.is_true(!(s->contains_edge(2, 4)));
        t.is_true(!(s->contains_edge(2, 5)));
        t.is_true(!(s->contains_edge(3, 4)));
        t.is_true( (s->contains_edge(3, 5))); // <-
        t.is_true(!(s->contains_edge(4, 5)));
        t.ok();
    }

    return 0;
}
