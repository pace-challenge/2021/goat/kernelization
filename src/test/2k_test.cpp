#include "../reduction_rule/reduce_2k.hpp"
#include "tester.hpp"
#include "../graph/dumb_weighted_graph.hpp"
#include "../computation/eval.hpp"
#include "../strategy/priority_strategy.hpp"
#include "../computation/tree.hpp"
#include "../node/node.hpp"
#include "../node/iter.hpp"
#include "../node/cherry.hpp"

using std::cerr;

int priority(const Node * n){
    (void)n;
    return 1;
}

Tester t;

int test_2k_reduction(WeightedGraph *graph, int expected_solution_size){
    // branching on cherries without the rule
    PriorityStrategy *strategy = new PriorityStrategy(priority);
    ComputationTree ct(strategy);
    Node * root = new IterNode(graph->copy(), Bound(0, 10), [](WeightedGraph *g, Bound bound){return new GreedyCherryNode(g, bound.upper);});
    Solution *solution = ct.evaluate(root);
    t.is_true((int)solution->weight() == expected_solution_size);

    // delete strategy;
    delete solution;

    // finding if rule is appliable
    pair<int, int> result_2k = find_2k_reducible_vertex(graph);
    if(result_2k.first == 1){
        // applying the rule
        int v = result_2k.second;
        TwokReduction * k_k = new TwokReduction(v);
        WeightedGraph * copy = graph->copy();
        Bound bound(0, 10);
        cerr << bound << endl;
        k_k->apply(copy, bound);
        cerr << bound << endl;
        // branching on reduced graph
        Node * root_2 = new IterNode(copy, bound, [](WeightedGraph *g, Bound bound){return new GreedyCherryNode(g, bound.upper);});
        //reconstricting solution
        CliqueSolution *solution_2 = ct.evaluate_clique(root_2);
        backtrack_solution(root_2, "");
        k_k->reconstruct_solution(solution_2);

        // the size of solutions should be same
        t.is_true(solution_2 != nullptr);
        t.is_true((int)solution_2->get_solution(graph)->weight() == expected_solution_size);
        // delete solution_2;
    }
    delete graph;
    t.ok();
    return result_2k.first;
}

void counterexample(){
    t.check("Checking corectness of 2k kernel reduction rule");
    WeightedGraph * g_336 = new DumbWeightedGraph(8);
    g_336->set_edge_weight(2, 0, 1);
    g_336->set_edge_weight(2, 1, -1);
    g_336->set_edge_weight(3, 0, FORBIDDEN_WEIGHT);
    g_336->set_edge_weight(3, 1, 1);
    g_336->set_edge_weight(3, 2, -1);
    g_336->set_edge_weight(4, 0, -1);
    g_336->set_edge_weight(4, 1, -1);
    g_336->set_edge_weight(4, 2, FORBIDDEN_WEIGHT);
    g_336->set_edge_weight(4, 3, 1);
    g_336->set_edge_weight(5, 0, 1);
    g_336->set_edge_weight(5, 1, -1);
    g_336->set_edge_weight(5, 2, 1);
    g_336->set_edge_weight(5, 3, -1);
    g_336->set_edge_weight(5, 4, -1);
    g_336->set_edge_weight(6, 0, -1);
    g_336->set_edge_weight(6, 1, 1);
    g_336->set_edge_weight(6, 2, -1);
    g_336->set_edge_weight(6, 3, -1);
    g_336->set_edge_weight(6, 4, -1);
    g_336->set_edge_weight(6, 5, -1);
    g_336->set_edge_weight(7, 0, -1);
    g_336->set_edge_weight(7, 1, 1);
    g_336->set_edge_weight(7, 2, 1);
    g_336->set_edge_weight(7, 3, -1);
    g_336->set_edge_weight(7, 4, -1);
    g_336->set_edge_weight(7, 5, -1);
    g_336->set_edge_weight(7, 6, -1);

    t.is_true(test_2k_reduction(g_336, 3) == 1);
    t.ok();
}

void counterexample_2(){
    t.check("Counterexample for the 2k-reduction 4.5.2021");
    WeightedGraph * graph = new DumbWeightedGraph(8);

    graph->set_edge_weight(1, 0, FORBIDDEN_WEIGHT);
    graph->set_edge_weight(2, 1, 1);
    graph->set_edge_weight(3, 0, FORBIDDEN_WEIGHT);
    graph->set_edge_weight(3, 1, FORBIDDEN_WEIGHT);
    graph->set_edge_weight(3, 2, FORBIDDEN_WEIGHT);
    graph->set_edge_weight(4, 0, 1);
    graph->set_edge_weight(4, 1, FORBIDDEN_WEIGHT);
    graph->set_edge_weight(4, 2, FORBIDDEN_WEIGHT);
    graph->set_edge_weight(4, 3, 0);
    graph->set_edge_weight(5, 0, FORBIDDEN_WEIGHT);
    graph->set_edge_weight(5, 1, 1);
    graph->set_edge_weight(5, 3, FORBIDDEN_WEIGHT);
    graph->set_edge_weight(5, 4, FORBIDDEN_WEIGHT);
    graph->set_edge_weight(6, 0, FORBIDDEN_WEIGHT);
    graph->set_edge_weight(6, 1, FORBIDDEN_WEIGHT);
    graph->set_edge_weight(6, 2, FORBIDDEN_WEIGHT);
    graph->set_edge_weight(6, 3, 2);
    graph->set_edge_weight(6, 4, 1);
    graph->set_edge_weight(6, 5, FORBIDDEN_WEIGHT);
    graph->set_edge_weight(7, 0, 1);
    graph->set_edge_weight(7, 1, FORBIDDEN_WEIGHT);
    graph->set_edge_weight(7, 2, 1);
    graph->set_edge_weight(7, 3, FORBIDDEN_WEIGHT);
    graph->set_edge_weight(7, 4, FORBIDDEN_WEIGHT);
    graph->set_edge_weight(7, 5, FORBIDDEN_WEIGHT);
    graph->set_edge_weight(7, 6, FORBIDDEN_WEIGHT);

    t.is_true(test_2k_reduction(graph, 3) == -1);

    t.ok();
}

int main(void){
    // counterexample();
    counterexample_2();

    return 0;
}
