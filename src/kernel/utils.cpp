#include "utils.hpp"
#include "../node/cherry.hpp"

vector<int> is_solution(WeightedGraph *graph, Solution *sol){
    WeightedGraph *copy = graph->copy();
    for(auto edge : sol->getSolution()) copy->flip_edge(edge.first, edge.second);
    auto v = find_cherry(copy);
    delete copy;
    return v;
}

using std::cin;
using std::cout;

void print_kernelization_metadata(int size, map<int,int> remapping, vector<tuple<int,int>> flipped_edges){
    cout << "c ";
    cout << size << ' ';
    cout << remapping.size() << ' ';
    for(auto p : remapping){
        cout << p.first << ' ';
        cout << p.second << ' ';
    }
    cout << flipped_edges.size() << ' ';
    for(auto p:flipped_edges){
        cout << std::get<0>(p) << ' ' << std::get<1>(p) << ' ';
    }
    cout << std::endl;
}

void load_kernelization_metadata(int &size, map<int,int> &remapping, vector<tuple<int,int>> & flipped_edges){
    string d;
    cin >> d; // c
    cin >> size;
    int mapping_size;
    cin >> mapping_size;
    for(int i=0; i<mapping_size; ++i){
        int a, b;
        cin >> a >> b;
        remapping[a] = b;
    }
    int flipped_size;
    cin >> flipped_size;
    for(int i=0; i<flipped_size; ++i){
        int a, b;
        cin >> a >> b;
        flipped_edges.push_back(std::make_tuple(a, b));
    }
}
