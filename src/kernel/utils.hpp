#pragma once

#include <vector>
#include "../graph/weighted_graph.hpp"
#include "../solution.hpp"

using std::vector;
using std::tuple;

vector<int> is_solution(WeightedGraph *graph, Solution *sol);

// methods for passing the essential data between the kernelized instance and the lifting algorithm
void print_kernelization_metadata(int size, map<int,int> remapping, vector<tuple<int,int>> flipped_edges);
void load_kernelization_metadata(int &size, map<int,int> &remapping, vector<tuple<int,int>> & flipped_edges);
