#include "node/node.hpp"
#include "graph/conversion.hpp"
#include "graph/weighted_graph.hpp"
#include "graph/materialized_weighted_graph.hpp"
#include "graph/dumb_weighted_graph.hpp"
#include "io/input_reader.hpp"
#include "solution.hpp"
#include "kernel/utils.hpp"

#include <iostream>
#include <vector>
#include <csignal>


using std::cin;
using std::cout;
using std::cerr;
using std::endl;
using std::vector;
using std::get;
using std::tuple;
using std::make_tuple;

void flip_solution_edges(Solution *s, vector<tuple<int,int>> &flipped_edges){
    for(auto p : flipped_edges){
        int a = get<0>(p);
        int b = get<1>(p);
        if(s->contains_edge(a, b)){
            s->remove_edge(a, b);
        }else{
            s->add_edge(a, b, 1);
        }
    }
}

// ====================================================================================================

int main ( void ) {
    string hash;

    InputReader in;

    auto graphFactory = [](int SZ){ return new DumbWeightedGraph(SZ); };
    Graph *g = in.read( cin );
    WeightedGraph * input_graph = convert_graph_to_weighted_graph(*g, graphFactory);
    delete g;

    cin >> hash;
    assert(hash == "#");

    int flipped_edges_count;
    cin >> flipped_edges_count;

    Graph *gg = in.read( cin );
    (void)gg; // (remove unused warning)

    // get the necessary information from the kernelization run
    int graph_size;
    map<int,int> remapping, inverse_remapping;
    vector<tuple<int,int>> flipped_edges;
    load_kernelization_metadata(graph_size, remapping, flipped_edges);
    for(auto &p : remapping) inverse_remapping[p.second] = p.first;

    // // this combined with assert(is_solution) was used to check solution validity
    // int N = input_graph->size();
    // WeightedGraph * reduced_graph = new DumbWeightedGraph(N);
    // for(int i = 0; i < (int)gg->get_vertices_count(); ++i) {
        // for(int j = 0; j < i; ++j) {
            // int weight = gg->contains_edge(j, i)*2-1;
            // // cerr << "ij: " << i << ' ' << j << endl;
            // assert(inverse_remapping.count(i));
            // assert(inverse_remapping.count(j));
            // // cerr << inverse_remapping[i] << ' ' << inverse_remapping[j] << endl;
            // reduced_graph->unsafe_set_edge_weight(inverse_remapping[i], inverse_remapping[j], weight);
        // }
    // }
    // delete gg;

    cin >> hash;
    assert(hash == "#");

    Solution * s = new Solution();
    int a, b;
    while(cin >> a >> b){
        --a;--b; // the solution is 1-indexed
        // the solution contains vertices of the remapped graph; we need to reconstruct the non-remapped solution by applying the remapping in reverse
        s->add_edge(inverse_remapping[a], inverse_remapping[b], 1);
    }

    {
        // assert(is_solution(reduced_graph, s).size()==0);
        flip_solution_edges(s, flipped_edges);

        // assert(is_solution(input_graph, s).size() == 0);
        auto sol_edges = s->getSolution();
        for(auto p : sol_edges){
            cout << (1+p.first) << ' ' << (1+p.second) << endl;
        }
    }

    delete input_graph;
    return 0;
}
